package com.voreni;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Log {
    private static boolean initialized = false;

    private static class ConsoleFormatter extends Formatter {
        @Override
        public String format(LogRecord record) {
            Object[] params = record.getParameters();
            String message;
            if (params != null) {
                MessageFormat mf = new MessageFormat(record.getMessage());
                message = mf.format(params);
            }
            else {
                message = record.getMessage();
            }

            Throwable exception = record.getThrown();
            if (exception != null) {
                StringWriter sw = new StringWriter();
                exception.printStackTrace(new PrintWriter(sw));

                return String.format("%1$s%n%2$s%n", message, sw.toString());
            }
            else {
                return String.format("%1$s%n", message);
            }
        }
    }

    private static class FileFormatter extends Formatter {
        @Override
        public String format(LogRecord record) {
            Object[] params = record.getParameters();
            String message;
            if (params != null) {
                MessageFormat mf = new MessageFormat(record.getMessage());
                message = mf.format(params);
            }
            else {
                message = record.getMessage();
            }

            Throwable exception = record.getThrown();
            if (exception != null) {
                StringWriter sw = new StringWriter();
                exception.printStackTrace(new PrintWriter(sw));

                return String.format("%1$tF %1$tT T%3$d %2$s: %4$s%n%5$s%n",
                    record.getMillis(),
                    record.getLevel(),
                    record.getThreadID(),
                    message,
                    sw.toString());
            }
            else {
                return String.format("%1$tF %1$tT T%3$d %2$s: %4$s%n",
                    record.getMillis(),
                    record.getLevel(),
                    record.getThreadID(),
                    message);
            }
        }
    }

    public static synchronized void initialize(String baseName) {
        Level level = Level.INFO;
        String logLevel = System.getenv("VORENI_LOG_LEVEL");
        if (logLevel != null) {
            logLevel = logLevel.toUpperCase();
            try {
                level = Level.parse(logLevel);
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        if (initialized) {
            Logger.getLogger(Log.class.getName()).config("Logging already initialized.");
        }
        initialized = true;

        LogManager.getLogManager().reset();

        // get the global logger and configure it
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.INFO); // log level for other code
        Logger voreniLogger = Logger.getLogger("com.voreni");
        voreniLogger.setLevel(level); // log level for our code

        ConsoleHandler consoleLog = new ConsoleHandler();
        consoleLog.setFormatter(new ConsoleFormatter());
        consoleLog.setLevel(Level.ALL);
        logger.addHandler(consoleLog);

        try {
            String logFilename = new File(baseName + ".%g.log").getAbsoluteFile().getCanonicalPath();
            FileHandler fileLog = new FileHandler(logFilename,
                2 * 1024 * 1024, // log file size before rotation
                10, // number of rotation logs
                true); // append to log
            fileLog.setFormatter(new FileFormatter());
            fileLog.setLevel(Level.ALL);
            logger.addHandler(fileLog);
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to create file log.", e);
        }
    }
}
