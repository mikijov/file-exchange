package com.voreni.fileserver.api;

import java.io.File;
import java.nio.file.Files;

import javax.xml.bind.DatatypeConverter;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbInterface;
import com.voreni.fileserver.DbSqlite;
import com.voreni.fileserver.Permission;
import com.voreni.fileserver.Service;
import com.voreni.fileserver.User;
import com.voreni.security.Security;

public class Helper {
    public static final String SYSADMIN = "sys.admin";
    public static final String USERNAME1 = "test.user.1";
    public static final String USERNAME2 = "test.user.2";
    public static final String USERNAME3 = "test.user.3";
    public static final String SERVICE1 = "com.voreni.service.1";
    public static final String SERVICE2 = "com.voreni.service.2";
    public static final String SERVICE3 = "com.voreni.service.3";

    public static final String PASSWORD = "aBigSecret919";

    public static final String SYSADMIN_SESSION =
        "Basic " + DatatypeConverter.printBase64Binary(
            (Helper.SYSADMIN + ":" + Helper.PASSWORD).getBytes());
    public static final String USER1_SESSION =
        "Basic " + DatatypeConverter.printBase64Binary(
            (Helper.USERNAME1 + ":" + Helper.PASSWORD).getBytes());
    public static final String USER2_SESSION =
        "Basic " + DatatypeConverter.printBase64Binary(
            (Helper.USERNAME2 + ":" + Helper.PASSWORD).getBytes());
    public static final String USER3_SESSION =
        "Basic " + DatatypeConverter.printBase64Binary(
            (Helper.USERNAME3 + ":" + Helper.PASSWORD).getBytes());

    private static boolean initialized = false;

    public static synchronized void initDb() throws Exception {
        if (initialized) {
            return;
        }
        initialized = true;

	    File file = new File("JUnitTest.sqlite");
	    if (file.exists()) {
	        Files.delete(file.toPath());
	    }

	    Db.setSingleton(new DbSqlite(file.getName(), true));
	    Db.createSchema();

	    User user = new User();
	    byte[] salt = Security.generateSalt(8);
	    user.setSalt(DatatypeConverter.printHexBinary(salt));
	    byte[] hashedPassword = Security.hashPassword(PASSWORD, salt);
	    user.setPassword(DatatypeConverter.printHexBinary(hashedPassword));

	    user.setUsername(SYSADMIN);
	    user.setSysAdmin(true);
	    user.setId(Db.createUser(user));

	    user.setUsername(USERNAME1);
	    user.setSysAdmin(false);
	    user.setId(Db.createUser(user));

	    user.setUsername(USERNAME2);
	    user.setId(Db.createUser(user));

	    user.setUsername(USERNAME3);
	    user.setId(Db.createUser(user));

	    Service service = new Service();
	    service.setName(SERVICE1);
	    Db.createService(service);
	    service.setName(SERVICE2);
	    Db.createService(service);
	    service.setName(SERVICE3);
	    Db.createService(service);

	    Permission permission = new Permission();
	    permission.setService(SERVICE1);
	    permission.setUsername(USERNAME1);
	    permission.setPermission(DbInterface.READ);
	    Db.createPermission(permission);
	    permission.setUsername(USERNAME2);
	    permission.setPermission(DbInterface.WRITE);
	    Db.createPermission(permission);
	    permission.setUsername(USERNAME3);
	    permission.setPermission(DbInterface.ADMIN);
	    Db.createPermission(permission);
    }

    public static ResourceConfig configure() {
        return new ResourceConfig()
                .packages("com.voreni.fileserver.api")
                .register(MyJacksonFeature.class)
                .register(MultiPartFeature.class)
                ;
    }
}
