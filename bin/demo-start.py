from fx import FileExchange

URL = 'http://localhost:8080/file-exchange/api/'

# upload first file
fx = FileExchange(URL, 'admin', 'password')
print "Uploading data.txt file to application.1.input..."
fx.upload("data.txt",
          "application.1.input/data.txt")
print "Done."
