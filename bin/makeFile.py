def makeFile(filename, size):
    with open(filename, "w") as f:
        curSize = 0
        while curSize < size:
            s = str(curSize)
            f.write(s)
            curSize = curSize + len(s)

makeFile("1kb.txt", 1024)
makeFile("10kb.txt", 10*1024)
makeFile("100kb.txt", 100*1024)
makeFile("1mb.txt", 1024*1024)
makeFile("10mb.txt", 10*1024*1024)
makeFile("100mb.txt", 100*1024*1024)
# makeFile("gb.txt", 1024*1024*1024)
