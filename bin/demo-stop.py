from fx import FileExchange

URL = 'http://localhost:8080/file-exchange/api/'

fx = FileExchange(URL, 'admin', 'password')
fx.deleteFile('application.1.input/data.txt')
fx.deleteFile('application.2.input/data.txt')
fx.deleteFile('application.3.input/data.txt')
