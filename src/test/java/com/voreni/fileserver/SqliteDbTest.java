package com.voreni.fileserver;

import static org.junit.Assert.*;

import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.voreni.fileserver.api.Helper;

// TODO: create tests to check query plans to check for usage of indexes

public class SqliteDbTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	    Helper.initDb();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	private static void createService(String name) throws SQLException {
	    Service service = new Service();
	    service.setName(name);
	    Db.createService(service);
	}

	@Test
	public void testCreateUser() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestCreateUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    Db.createUser(user);
	}

	@Test(expected=SQLException.class)
	public void testFailToCreateDuplicateUser() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestFailToCreateDuplicateUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    try {
	        Db.createUser(user);
	    }
	    catch (SQLException e) {
	        e.printStackTrace();
	        fail("Unexpectedly failed to create user.");
	    }

	    Db.createUser(user);
	}

	@Test
	public void testDeleteUserById() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestDeleteById");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    user.setId(Db.createUser(user));

	    Db.deleteUser(user.getId());
	}

	@Test
	public void testDeleteUserByUsername() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestDeleteByUsername");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    user.setId(Db.createUser(user));

	    Db.deleteUser(user.getUsername());
	}

	@Test(expected=SQLException.class)
	public void testFailDeleteUserById() throws SQLException {
	    Db.getUser(-1);
	}

	@Test(expected=SQLException.class)
	public void testFailDeleteUserByUsername() throws SQLException {
	    Db.getUser("SqliteTestFailDeleteByUsername");
	}

	@Test
	public void testCreateUserAfterDelete() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestCreateAfterDelete");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    long id = Db.createUser(user);
	    Db.deleteUser(id);
	    long id2 = Db.createUser(user);
	    assertNotEquals(id, id2);
	}

	@Test
	public void testGetUserById() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestGetById");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    user.setId(Db.createUser(user));

	    User user2 = Db.getUser(user.getId());

	    assertEquals(user, user2);
	}

	@Test
	public void testGetUserByUsername() throws SQLException {
	    User user = new User();
	    user.setUsername("SqliteTestGetByUsername");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    user.setId(Db.createUser(user));

	    User user2 = Db.getUser("SqliteTestGetByUsername");

	    assertEquals(user, user2);
	}

	@Test(expected=SQLException.class)
	public void testFailGetUserById() throws SQLException {
	    Db.getUser(-1);
	}

	@Test(expected=SQLException.class)
	public void testFailGetUserByUsername() throws SQLException {
	    Db.getUser("SqliteTestFailGetByUsername");
	}

	@Test
	public void testCreateService() throws SQLException {
	    createService("testCreateService");
	}

	@Test(expected=SQLException.class)
	public void testFailCreateServiceBadName() throws SQLException {
	    createService(null);
	}

	@Test(expected=SQLException.class)
	public void testCreateDuplicateService() throws SQLException {
	    createService("testCreateDuplicateService");
	    createService("testCreateDuplicateService");
	}

	@Test
	public void testDeleteService() throws SQLException {
	    createService("testDeleteService");
	    Db.deleteService("testDeleteService");
	}

	@Test(expected=SQLException.class)
	public void testFailDeleteNonExistentService() throws SQLException {
	    Db.deleteService("testFailDeleteNonExistentService");
	}

	@Test
	public void testCreateServiceAfterDelete() throws SQLException {
	    createService("testCreateServiceAfterDelete");
	    Db.deleteService("testCreateServiceAfterDelete");
	    createService("testCreateServiceAfterDelete");
	}

	@Test
	public void testAuthorizeUser() throws SQLException {
	    User user = new User();
	    user.setUsername("testAuthorizeUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testAuthorizeUser");

	    Db.createPermission(new Permission("testAuthorizeUser", "testAuthorizeUser", DbInterface.READ));
	}

	@Test(expected=SQLException.class)
	public void testFailAuthorizeBadUser() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailAuthorizeBadUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailAuthorizeBadUser");

	    Db.createPermission(new Permission("testFailAuthorizeBadUser", "bad", DbInterface.READ));
	}

	@Test(expected=SQLException.class)
	public void testFailAuthorizeBadService() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailAuthorizeBadUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailAuthorizeBadUser");

	    Db.createPermission(new Permission("bad", "testFailAuthorizeBadUser", DbInterface.READ));
	}

	@Test(expected=SQLException.class)
	public void testFailAuthorizeBadPrivilege() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailAuthorizeBadPrivilege");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailAuthorizeBadPrivilege");

	    Db.createPermission(new Permission("testFailAuthorizeBadPrivilege", "testFailAuthorizeBadPrivilege", 44));
	}

	@Test
	public void testDeauthorizeUser() throws SQLException {
	    User user = new User();
	    user.setUsername("testDeauthorizeUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testDeauthorizeUser");

	    Db.createPermission(new Permission("testDeauthorizeUser", "testDeauthorizeUser", DbInterface.READ));
	    Db.deletePermission(new Permission("testDeauthorizeUser", "testDeauthorizeUser", DbInterface.NONE));
	}

	@Test(expected=SQLException.class)
	public void testFailDeauthorizeBadUser() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailDeauthorizeBadUser");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailDeauthorizeBadUser");

	    Db.createPermission(new Permission("testFailDeauthorizeBadUser", "testFailDeauthorizeBadUser", DbInterface.READ));
	    Db.deletePermission(new Permission("testFailDeauthorizeBadUser", "bad", DbInterface.NONE));
	}

	@Test(expected=SQLException.class)
	public void testFailDeauthorizeBadService() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailDeauthorizeBadService");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailDeauthorizeBadService");

	    Db.createPermission(new Permission("testFailDeauthorizeBadService", "testFailDeauthorizeBadService", DbInterface.READ));
	    Db.deletePermission(new Permission("bad", "testFailDeauthorizeBadService", DbInterface.NONE));
	}

	@Test
	public void testCreateFile() throws SQLException {
	    User user = new User();
	    user.setUsername("testCreateFile");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testCreateFile");

	    Db.createPermission(new Permission("testCreateFile", "testCreateFile", DbInterface.READ));

	    File file = new File();
	    file.setFilename("testCreateFile");
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary("testCreateFile".getBytes()));
	    file.setSha1("testCreateFile");
	    file.setTimeCreated(DateTime.now());

	    Db.createFile("testCreateFile", file);
	}

	@Test(expected=SQLException.class)
	public void testFailCreateFileBadService() throws SQLException {
	    User user = new User();
	    user.setUsername("testFailCreateFileBadService");
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService("testFailCreateFileBadService");

	    Db.createPermission(new Permission("testFailCreateFileBadService", "testFailCreateFileBadService", DbInterface.READ));

	    File file = new File();
	    file.setFilename("testFailCreateFileBadService");
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary("testFailCreateFileBadService".getBytes()));
	    file.setSha1("testFailCreateFileBadService");
	    file.setTimeCreated(DateTime.now());

	    Db.createFile("badService", file);
	}

	@Test(expected=SQLException.class)
	public void testFailCreateFileMissingField1() throws SQLException {
	    final String testName = "testFailCreateFileMissingField1";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
//	    file.setOriginalFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);
	}

	@Test(expected=SQLException.class)
	public void testFailCreateFileMissingField2() throws SQLException {
	    final String testName = "testFailCreateFileMissingField2";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
//	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);
	}

	@Test(expected=SQLException.class)
	public void testFailCreateFileMissingField3() throws SQLException {
	    final String testName = "testFailCreateFileMissingField3";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
//	    file.setChecksum(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);
	}

	@Test(expected=SQLException.class)
	public void testFailCreateFileMissingField4() throws SQLException {
	    final String testName = "testFailCreateFileMissingField4";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
//	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);
	}

	@Test
	public void testGetFile() throws SQLException {
	    final String testName = "testGetFile";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);

	    File file2 = Db.getFile(testName, testName);

	    assertEquals(file, file2);
	}

	@Test(expected=SQLException.class)
	public void testFailGetFile() throws SQLException {
	    final String testName = "testFailGetFile";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);

	    Db.getFile(testName, "badFilename");
	}

	@Test
	public void testDeleteFile() throws SQLException {
	    final String testName = "testDeleteFile";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);

	    Db.deleteFile(testName, testName);

	    try {
	        Db.getFile(testName, testName);
	        fail();
	    }
	    catch (SQLException e) {
	        assertTrue(Db.isNotFoundError(e));
	    }
	}

	@Test(expected=SQLException.class)
	public void testFailDeleteFile() throws SQLException {
	    final String testName = "testFailDeleteFile";
	    User user = new User();
	    user.setUsername(testName);
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");
	    user.setId(Db.createUser(user));

	    createService(testName);

	    Db.createPermission(new Permission(testName, testName, DbInterface.READ));

	    File file = new File();
	    file.setFilename(testName);
	    file.setCompressedFilename(DatatypeConverter.printBase64Binary(testName.getBytes()));
	    file.setSha1(testName);
	    file.setTimeCreated(DateTime.now());

	    Db.createFile(testName, file);

	    Db.deleteFile(testName, "badFilename");
	}

	@Test
	public void testCreateMonitor() throws SQLException {
	    Monitor monitor = new Monitor();
	    monitor.setName("testCreateMonitor");
	    monitor.setStartOffset(Duration.ZERO);
	    monitor.setFeeds("[{item:value},{item:value2}]");

	    Db.createMonitor(monitor);
	}

	@Test
	public void testDeleteMonitor() throws SQLException {
	    Monitor monitor = new Monitor();
	    monitor.setName("testCreateMonitor");
	    monitor.setStartOffset(Duration.ZERO);
	    monitor.setFeeds("[{item:value},{item:value2}]");

	    Db.createMonitor(monitor);

	    Db.deleteMonitor(monitor.getName());
	}

//	@Test
//	public void testMonitor() throws SQLException {
//	    Monitor monitor = new Monitor();
//	    monitor.setName("testCreateMonitor");
//	    monitor.setStartOffset(Duration.ZERO);
//	    monitor.setFeeds("[{item:value},{item:value2}]");
//
//	    Db.createMonitor(monitor);
//
//	    Db.deleteMonitor(monitor.getName());
//	}
}
