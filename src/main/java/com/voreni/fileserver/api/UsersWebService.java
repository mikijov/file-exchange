package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.User;
import com.voreni.security.Security;

@Path("users")
public class UsersWebService {
	private final static Logger LOGGER = Logger.getLogger(UsersWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    private void checkUsername(String username) {
        if (!Pattern.matches("^[a-zA-Z0-9\\.\\-\\_]+$", username)) {
            throw new WebApplicationException(
                "Username must contain only a-z A-Z 0-9 . - _ characters.",
                Response.Status.BAD_REQUEST);
        }
    }

    private void checkPassword(String password) {
        if (Security.calculatePasswordStrength(password) < 1) {
            throw new WebApplicationException(
                "Password is too simple. Consider using longer password, mixed case, punctuation, and avoid duplicated or characters in a sequence.",
                Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listUsers(
        @QueryParam("pattern") final String pattern
        )
    {
        LOGGER.fine("listUsers()");

        User authUser = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(authUser,  Auth.Authorization.SYSADMIN);

        final Pattern regex = Ws.compilePattern(pattern);

        try {
            final DbIterator<User> iter = Db.getUsers(null);
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            User user = iter.next();
                            if (regex == null || regex.matcher(user.getUsername()).find()) {
                                jg.writeObject(user);
                            }
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listUsers(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            Audit.log(authUser, ip, Audit.USER_LIST, pattern);

            LOGGER.fine("listUsers(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @POST
    public void createUser(
        User newUser,
        @QueryParam("password") String password
        )
    {
        LOGGER.fine("createUser()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        checkUsername(newUser.getUsername());
        if (password != null) {
            checkPassword(password);
        }

        try {
            byte[] salt = newUser.generateSalt();

            if (password != null) {
                byte[] hashedPassword = Security.hashPassword(password, salt);
                newUser.setPassword(DatatypeConverter.printHexBinary(hashedPassword));
            }
            else {
                newUser.setPassword(null);
            }

            Db.createUser(newUser);

            Audit.log(user, ip, Audit.USER_CREATE, newUser.getUsername(), newUser.isSysAdmin() ? 1 : 0);

            LOGGER.fine("createUser(" + newUser.getUsername() + ") OK");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @GET
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(
        @PathParam("username") String username
        )
    {
        LOGGER.fine("getUser()");

        User authUser = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(authUser, username, Auth.Authorization.SELF);

        try {
            User user = Db.getUser(username);

            Audit.log(authUser, ip, Audit.USER_READ, user.getUsername());

            return user;
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @PUT
    @Path("{username}")
    public void updateUser(
        @PathParam("username") String username,
        User user
        )
    {
        LOGGER.fine("updateUser()");

        User authUser = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(authUser, Auth.Authorization.SYSADMIN);

        checkUsername(username);

        try {
            User oldUser = Db.getUser(username);

            oldUser.setUsername(user.getUsername());
            oldUser.setSysAdmin(user.isSysAdmin());

            Db.updateUser(username, oldUser);

            Audit.log(authUser, ip, Audit.USER_UPDATE, username, user.getUsername(),
                user.isSysAdmin() ? 1 : 0);

            LOGGER.fine("updateUser(" + username + ") OK");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @DELETE
    @Path("{username}")
    public void deleteUser(
        @PathParam("username") String username
        )
    {
        LOGGER.fine("deleteUser()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        try {
            Db.deleteUser(username);

            Audit.log(user, ip, Audit.USER_DELETE, username);

            LOGGER.fine("deleteUser(" + username + ") OK");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @PUT
    @Path("{username}/password")
    public void changePassword(
        @PathParam("username") String username,
        @QueryParam("password") String password
        )
    {
        LOGGER.fine("changePassword()");

        User authUser = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkUserPermission(authUser, username, Auth.Authorization.SELF);

        if (password == null) {
            throw new WebApplicationException(
                "No password provided.",
                Response.Status.BAD_REQUEST);
        }
        checkPassword(password);

        try {
            User user = Db.getUser(username);

            checkPassword(password);
            byte[] salt = DatatypeConverter.parseHexBinary(user.getSalt());
            byte[] hashedPassword = Security.hashPassword(password, salt);
            user.setPassword(DatatypeConverter.printHexBinary(hashedPassword));

            Db.updateUser(username, user);

            Audit.log(authUser, ip, Audit.USER_PASSWORD, username);

            LOGGER.fine("changePassword(" + username + ") OK");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

}
