package com.voreni.fileserver;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Random;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.voreni.fileserver.api.Helper;

// TODO: create tests to check query plans to check for usage of indexes

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SqliteDbLoadTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	    Helper.initDb();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	private static final int USER_COUNT = 1000;
	private static final int SERVICE_COUNT = 100;
	private static final int FILE_PER_DAY_PER_SERVICE = 20;
	private static final int DAYS = 20;

	@Test
	public void test1creatingServices() throws SQLException {
	    Service service = new Service();
	    for (int i = 0; i < SERVICE_COUNT; ++i) {
	        service.setName("test.service." + i);
	        Db.createService(service);
	    }
	}

	@Test
	public void test2creatingUsers() throws SQLException {
	    User user = new User();
	    user.setSalt("0011223344556677");
	    user.setPassword("PPAASSWWOORRDD");

	    Permission permission = new Permission();
	    permission.setPermission(DbInterface.ADMIN);

	    for (int i = 0; i < USER_COUNT; ++i) {
	        String username = "user." + i;
	        user.setUsername(username);
	        Db.createUser(user);

	        permission.setService("test.service." + (i % SERVICE_COUNT));
	        permission.setUsername(username);
	        Db.createPermission(permission);
	    }
	}

	@Test
	public void test3creatingFiles() throws SQLException {
	    Random rnd = new Random(System.currentTimeMillis());

	    File file = new File();
	    file.setCompressedFilename("00112233445566778899AABBCCDDEEFFGGHHIIJJKK.gz");
	    file.setSha1("0123456789ABCDEFGHIJ");
	    file.setTimeCreated(DateTime.now());
	    for (int day = 0; day < DAYS; ++day) {
	        long startTime = System.currentTimeMillis();

	        String filename = "file.name.of.some.length" + day;
	        for (int service = 0; service < SERVICE_COUNT; ++service) {
	            String serviceName = "test.service." + service;
	            for (int fileCount = 0; fileCount < FILE_PER_DAY_PER_SERVICE; ++fileCount) {
	                file.setFilename(filename + "." + fileCount);
	                file.setFileSize(rnd.nextInt(500000));
	                Db.createFile(serviceName, file);
	            }
	        }

	        System.out.println("Day " + day + " finished in "
	                        + ((System.currentTimeMillis() - startTime) / 1000) + " seconds.");
	    }
	}
}
