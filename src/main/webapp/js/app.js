(function() {

  "use strict";

  // Declare app level module which depends on views, and components
  angular.module("fileExchange", [
    "ngRoute",
    "fxLogin",
    "fxDashboard",
    "fxUsers",
    "fxUserDetails",
    "fxServices",
    "fxFiles",
    "fxMonitors",
    "fxMonitorDetails",
    "fxAudit",
  ])

  .run(["$rootScope", "$templateCache", "$log", function($rootScope, $templateCache, $log) {
    $rootScope.$on('$includeContentLoaded',function(event,url){
      if (url == "header.html") {
        $.AdminLTE.pushMenu.activate("[data-toggle='offcanvas']");
      }
      else if (url == "sidebar.html") {
        //Enable sidebar tree view controls
        $.AdminLTE.tree('.sidebar');
      }
    });

    $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
      $log.info("Template cache cleared.");

      $.AdminLTE.layout.fix();
      $.AdminLTE.layout.fixSidebar();
      $.AdminLTE.boxWidget.activate();
    });
  }])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.otherwise({redirectTo: "/"});
  }])

  .controller("fxCtrl", ["$scope", "$rootScope", "$log", "fxApi", 
              function($scope, $rootScope, $log, fxApi) {
    $scope.onSignOut = function() {
      fxApi.signOut();
    };

    $rootScope.user = null;
    fxApi.checkSession()
    .then(
      function(user) {
        $rootScope.user = user;
      },
      function(response) {
        fxApi.signOut();
      }
    );
  }])

  .factory("fxApi", ["$rootScope", "$location", "$http", "Base64", "$log", "$q",
           function($rootScope, $location, $http, Base64, $log, $q) {
    var api = {};

    api.checkSession = function() {
      $log.info("checkSession");
      var authorization = sessionStorage.getItem("fx-session");
      return api.getLoggedInUser(authorization);
    };

    api.signIn = function(username, password) {
      $log.info("signIn");
      var authorization = "Basic " + Base64.encode(username + ":" + password);
      return api.getLoggedInUser(authorization);
    };

    api.signOut = function() {
      $log.info("signOut");
      $rootScope.user = null;
      delete $http.defaults.headers.common["Authorization"];
      sessionStorage.removeItem("fx-session");
      $location.url("/login");
    };

    api.getLoggedInUser = function(authorization) {
      $log.info("getLoggedInUser");
      return $http({
        method: "GET",
        headers: {
          "Authorization": authorization
        },
        url: "/file-exchange/api/session/user",
      })
      .then(
        function(response) {
          $rootScope.user = response.data;

          $http.defaults.headers.common["Authorization"] = authorization;
          sessionStorage.setItem("fx-session", authorization);
          
          $log.info("User:", $rootScope.user);
          return $rootScope.user;
        },
        function(response) {
          delete $http.defaults.headers.common["Authorization"];
          sessionStorage.removeItem("fx-session");
          return $q.reject(response);
        }
      );
    };

    api.getUsers = function(pattern) {
      $log.info("getUsers");
      var url = "/file-exchange/api/users";
      if (pattern) {
        url += "?pattern=" + window.encodeURIComponent(pattern);
      }
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getUsers OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.getUser = function(username) {
      $log.info("getUser", username);
      var url = "/file-exchange/api/users/" + window.encodeURIComponent(username);
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getUser OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.createUser = function(user, password) {
      $log.info("createUser", user.username);

      var url = "/file-exchange/api/users";
      if (password) {
        url += "?password=" + window.encodeURIComponent(password);
      }
      return $http({
        method: "POST",
        url: url,
        data: user,
      })
      .then(
        function(response) {
          $log.info("createUser OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("createUser Failed");
          return $q.reject(response);
        }
      );
    };

    api.updateUser = function(username, user) {
      $log.info("updateUser", username);

      var url = "/file-exchange/api/users/" + window.encodeURIComponent(username);
      return $http({
        method: "PUT",
        url: url,
        data: user,
      })
      .then(
        function(response) {
          $log.info("updateUser OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("updateUser Failed");
          return $q.reject(response);
        }
      );
    };

    api.deleteUser = function(username) {
      $log.info("deleteUser");

      var url = "/file-exchange/api/users/" + 
        window.encodeURIComponent(username);
      return $http({
        method: "DELETE",
        url: url,
      })
      .then(
        function(response) {
          $log.info("deleteUser OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("deleteUser Failed");
          return $q.reject(response);
        }
      );
    };

    api.getUserPermissions = function(username) {
      $log.info("getUserPermissions");
      var url = "/file-exchange/api/permissions?username=" + window.encodeURIComponent(username);
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getUserPermissions OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.createPermission = function(permission) {
      $log.info("createPermission");

      var url = "/file-exchange/api/permissions";
      return $http({
        method: "POST",
        url: url,
        data: permission,
      })
      .then(
        function(response) {
          $log.info("createPermission OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("createPermission Failed");
          return $q.reject(response);
        }
      );
    };

    api.deletePermission = function(permission) {
      $log.info("deletePermission");

      var url = "/file-exchange/api/permissions";
      return $http({
        method: "DELETE",
        url: url,
        data: permission
      })
      .then(
        function(response) {
          $log.info("deletePermission OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("deletePermission Failed");
          return $q.reject(response);
        }
      );
    };

    api.getServices = function(pattern) {
      var url = "/file-exchange/api/services";
      if (pattern) {
        url += "?pattern=" + window.encodeURIComponent(pattern);
      }
      $log.info("getServices:", url);
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getServices OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.createService = function(service) {
      $log.info("createService");

      var url = "/file-exchange/api/services";
      return $http({
        method: "POST",
        url: url,
        data: service,
      })
      .then(
        function(response) {
          $log.info("createService OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("createService Failed");
          return $q.reject(response);
        }
      );
    };

    api.updateService = function(serviceName, service) {
      $log.info("updateService");

      var url = "/file-exchange/api/services/" + window.encodeURIComponent(serviceName);
      return $http({
        method: "PUT",
        url: url,
        data: service,
      })
      .then(
        function(response) {
          $log.info("updateService OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("updateService Failed");
          return $q.reject(response);
        }
      );
    };

    api.deleteService = function(serviceName) {
      $log.info("deleteService");

      var url = "/file-exchange/api/services/" + window.encodeURIComponent(serviceName);
      return $http({
        method: "DELETE",
        url: url,
      })
      .then(
        function(response) {
          $log.info("deleteService OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("deleteService Failed");
          return $q.reject(response);
        }
      );
    };

    api.getFiles = function(service, pattern, fromTime, toTime) {
      var url = "/file-exchange/api/services/" + window.encodeURIComponent(service) + "/files";
      var first = true;
      if (pattern) {
        url += first ? "?" : "&";
        first = false;
        url += "pattern=" + window.encodeURIComponent(pattern);
      }
      if (fromTime) {
        url += first ? "?" : "&";
        first = false;
        url += "fromTime=" + window.encodeURIComponent(fromTime);
      }
      if (toTime) {
        url += first ? "?" : "&";
        first = false;
        url += "toTime=" + window.encodeURIComponent(toTime);
      }
      $log.info("getFiles:", url);
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getFiles OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.deleteFile = function(serviceName, filename) {
      $log.info("deleteService");

      var url = "/file-exchange/api/services/" + 
        window.encodeURIComponent(serviceName) +
        "/files/" +
        window.encodeURIComponent(filename);
      return $http({
        method: "DELETE",
        url: url,
      })
      .then(
        function(response) {
          $log.info("deleteFile OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("deleteFile Failed");
          return $q.reject(response);
        }
      );
    };

    api.getStats = function() {
      $log.info("getStats");
      var url = "/file-exchange/api/stats";
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getStats OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.getFileUri = function(service, filename) {
      var url = "/file-exchange/api/services/" + window.encodeURIComponent(service) + "/files/" + window.encodeURIComponent(filename);
      // url += "?token=" + encodeURIComponent($rootScope.session.token);
      return url;
    };

    api.getMonitors = function(pattern) {
      var url = "/file-exchange/api/monitors";
      if (pattern) {
        url += "?pattern=" + window.encodeURIComponent(pattern);
      }
      $log.info("getMonitors:", url);

      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getMonitors OK");
          var monitors = response.data;
          for (var monitor of monitors) {
            $log.info("Feeds:", monitor.feeds);
            monitor.feeds = angular.fromJson(monitor.feeds);
            $log.info("Feeds2:", monitor.feeds);
          }
          return monitors;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.getMonitor = function(monitorName) {
      $log.info("getMonitor");

      var url = "/file-exchange/api/monitors/" + window.encodeURIComponent(monitorName);
      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getMonitor OK");
          var monitor = response.data;
          $log.info("Feeds:", monitor.feeds);
          monitor.feeds = angular.fromJson(monitor.feeds);
          return monitor;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    api.createMonitor = function(monitor) {
      var m = angular.fromJson(angular.toJson(monitor)); // deep copy
      m.feeds = angular.toJson(m.feeds);

      $log.info("createMonitor");

      var url = "/file-exchange/api/monitors";
      return $http({
        method: "POST",
        url: url,
        data: m,
      })
      .then(
        function(response) {
          $log.info("createMonitor OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("createMonitor Failed");
          return $q.reject(response);
        }
      );
    };

    api.updateMonitor = function(monitorName, monitor) {
      var m = angular.fromJson(angular.toJson(monitor)); // deep copy
      m.feeds = angular.toJson(m.feeds);

      $log.info("updateMonitor");

      var url = "/file-exchange/api/monitors/" + window.encodeURIComponent(monitorName);
      return $http({
        method: "PUT",
        url: url,
        data: m,
      })
      .then(
        function(response) {
          $log.info("updateMonitor OK");
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          $log.info("updateMonitor Failed");
          return $q.reject(response);
        }
      );
    };

    api.getAudit = function(pattern) {
      var url = "/file-exchange/api/audit";
      if (pattern) {
        url += "?pattern=" + window.encodeURIComponent(pattern);
      }
      $log.info("getAudit:", url);

      return $http({
        method: "GET",
        url: url,
      })
      .then(
        function(response) {
          $log.info("getAudit OK");
          var audit = response.data;
          for (var entry of audit) {
            entry.timestamp = new Date(entry.timestamp);
          }
          return response.data;
        },
        function(response) {
          if (response.status == 401) {
            api.signOut();
          }
          return $q.reject(response);
        }
      );
    };

    return api;
  }]) // factory

  /*
   * bytes filter
   */

  .filter('bytes', function() {
    return function(bytes, precision) {
      if (bytes === 0) { return '0'; }
      if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
      if (typeof precision === 'undefined') precision = 1;

      var units = ['', 'KB', 'MB', 'GB', 'TB', 'PB'],
        number = Math.floor(Math.log(bytes) / Math.log(1024)),
        val = (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision);

      return  (val.match(/\.0*$/) ? val.substr(0, val.indexOf('.')) : val) +  ' ' + units[number];
    };
  })
  .filter('bps', function() {
    return function(bytes, precision) {
      if (bytes === 0) { return '0 bps'; }
      if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
      if (typeof precision === 'undefined') precision = 1;

      var units = ['bps', 'KB/s', 'MB/s', 'GB/s', 'TB/s', 'PB/s'],
        number = Math.floor(Math.log(bytes) / Math.log(1024)),
        val = (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision);

      return  (val.match(/\.0*$/) ? val.substr(0, val.indexOf('.')) : val) +  ' ' + units[number];
    };
  })
  .filter('action', function() {
    return function(action) {
      switch (action) {
        case 10: return "Create User";
        case 11: return "Read User";
        case 12: return "Update User";
        case 13: return "Delete User";
        case 14: return "List Users";
        case 15: return "Change Password";

        case 20: return "Create Service";
        case 21: return "Read Service";
        case 22: return "Update Service";
        case 23: return "Delete Service";
        case 24: return "List Services";

        case 30: return "Create Permission";
        case 31: return "Read Permission";
        case 32: return "Update Permission";
        case 33: return "Delete Permission";
        case 34: return "List Permissions";

        case 40: return "Create File";
        case 41: return "Read File";
        case 42: return "Update File";
        case 43: return "Delete File";
        case 44: return "List Files";

        case 50: return "Create Monitor";
        case 51: return "Read Monitor";
        case 52: return "Update Monitor";
        case 53: return "Delete Monitor";
        case 54: return "List Monitors";

        case 94: return "List Audit";

        default: return "unknown";
      }
    };
  })

  /*
   * Base64
   */

  .factory('Base64', function() {
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    return {
      encode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        do {
          chr1 = input.charCodeAt(i++);
          chr2 = input.charCodeAt(i++);
          chr3 = input.charCodeAt(i++);

          enc1 = chr1 >> 2;
          enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
          enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
          enc4 = chr3 & 63;

          if (isNaN(chr2)) {
            enc3 = enc4 = 64;
          } else if (isNaN(chr3)) {
            enc4 = 64;
          }

          output = output +
            keyStr.charAt(enc1) +
            keyStr.charAt(enc2) +
            keyStr.charAt(enc3) +
            keyStr.charAt(enc4);
          chr1 = chr2 = chr3 = "";
          enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);

        return output;
      },

      decode: function (input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
          alert("There were invalid base64 characters in the input text.\n" +
                "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
          enc1 = keyStr.indexOf(input.charAt(i++));
          enc2 = keyStr.indexOf(input.charAt(i++));
          enc3 = keyStr.indexOf(input.charAt(i++));
          enc4 = keyStr.indexOf(input.charAt(i++));

          chr1 = (enc1 << 2) | (enc2 >> 4);
          chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
          chr3 = ((enc3 & 3) << 6) | enc4;

          output = output + String.fromCharCode(chr1);

          if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
          }
          if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
          }

          chr1 = chr2 = chr3 = "";
          enc1 = enc2 = enc3 = enc4 = "";

        } while (i < input.length);

        return output;
      }
    };
  });

})();
