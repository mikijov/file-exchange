(function() {

  "use strict";

  angular.module("fxLogin", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/login", {
      templateUrl: "login.html",
      controller: "loginCtrl"
    });
  }])

  .controller("loginCtrl", ["$scope", "$log", "fxApi", "$location", 
              function($scope, $log, fxApi, $location) 
  {
    $scope.$parent.viewName = "login";
    $scope.username = "";
    $scope.password = "";
    
    $scope.onSignIn = function() {
      fxApi.signIn($scope.username, $scope.password)
      .then(
        function(session) {
          $("#callout-invalid-login").addClass("hidden");
          $location.url("/dashboard");
        },
        function(error) {
          $("#callout-invalid-login").removeClass("hidden");
        }
      );
    };

    $('[ng-model="username"]').focus();
  }]);

})();
