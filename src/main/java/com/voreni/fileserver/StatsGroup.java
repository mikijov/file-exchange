package com.voreni.fileserver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public class StatsGroup {
    @JsonAutoDetect(
        creatorVisibility=JsonAutoDetect.Visibility.ANY,
        fieldVisibility=JsonAutoDetect.Visibility.NONE,
        getterVisibility=JsonAutoDetect.Visibility.NONE,
        isGetterVisibility=JsonAutoDetect.Visibility.NONE,
        setterVisibility=JsonAutoDetect.Visibility.NONE
        )
    private static class Range {
        @JsonProperty
        private String name;
        @JsonProperty
        private long[] samples;
        @JsonProperty
        private long total = 0;

        private int sampleCount; // number of samples before asking to push

        public Range(String name, int sampleCount) {
            this.name = name;
            samples = new long[sampleCount];
            this.sampleCount = samples.length;
        }

        public boolean push(long value) {
            total += value - samples[0];
            for (int i = 0; i < samples.length - 1; ++i) {
                samples[i] = samples[i + 1];
            }
            samples[samples.length - 1] = value;
            return --sampleCount > 0;
        }

        public long getAndReset() {
            sampleCount = samples.length;
            return total;
        }
    }

    private AtomicLong current = new AtomicLong();
    @JsonProperty
    private List<Range> ranges = new ArrayList<Range>();

    public StatsGroup(int days) {
        createRange("seconds", 60);
        createRange("minutes", 60);
        createRange("hours", 24);
        createRange("days", days);
    }

    public void createRange(String name, int sampleCount) {
        ranges.add(new Range(name, sampleCount));
    }

    public void add(long val) {
        current.addAndGet(val);
    }

    public void push() {
        long value = current.getAndSet(0);

        for (Range range: ranges) {
            boolean stop = range.push(value);
            if (stop) {
                break;
            }
            value = range.getAndReset();
        }
    }
}
