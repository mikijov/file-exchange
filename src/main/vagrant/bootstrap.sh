#!/usr/bin/env bash

apt-get update
apt-get --yes upgrade

mkdir -p /vagrant/puppet/modules
if [ -d /vagrant/puppet/modules/firewall ]; then
  puppet module --modulepath /vagrant/puppet/modules upgrade puppetlabs-firewall
else
  puppet module --modulepath /vagrant/puppet/modules install puppetlabs-firewall
fi
if [ -d /vagrant/puppet/modules/firewall ]; then
  puppet module --modulepath /vagrant/puppet/modules upgrade puppetlabs-java
else
  puppet module --modulepath /vagrant/puppet/modules install puppetlabs-java
fi
if [ -d /vagrant/puppet/modules/firewall ]; then
  puppet module --modulepath /vagrant/puppet/modules upgrade puppetlabs-tomcat
else
  puppet module --modulepath /vagrant/puppet/modules install puppetlabs-tomcat
fi
