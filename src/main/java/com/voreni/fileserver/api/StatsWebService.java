package com.voreni.fileserver.api;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;

import com.voreni.fileserver.Stats;

@Path("stats")
public class StatsWebService {
	private final static Logger LOGGER = Logger.getLogger(StatsWebService.class.getName());

	@Context
	private Request request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Stats getStats()
    {
        LOGGER.fine("getStats()");

        return Stats.getSingleton();
    }
}
