package com.voreni.fileserver;

import org.joda.time.Duration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public class Monitor
{
    private long id;
    @JsonProperty
    private String name;

    /**
     * Time since midnight when to start monitoring every day.
     */
    @JsonProperty
    private Duration startOffset;

    @JsonProperty
    private String feeds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duration getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(Duration startOffset) {
        this.startOffset = startOffset;
    }

    public String getFeeds() {
        return feeds;
    }

    public void setFeeds(String feeds) {
        this.feeds = feeds;
    }
}

