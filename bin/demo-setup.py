from fx import FileExchange
import time

URL = 'http://localhost:8080/file-exchange/api/'

fx = FileExchange(URL, 'admin', 'password')
# fx.createUser("application.user.1", 'password')
# fx.createUser("application.user.2", 'password')
# fx.createUser("application.user.3", 'password')
# fx.createUser("it.support", 'password')
#
# fx.createService("application.1.input")
# fx.createPermission("application.1.input", "application.user.1", "R")
#
# fx.createService("application.2.input")
# fx.createPermission("application.2.input", "application.user.1", "W")
# fx.createPermission("application.2.input", "application.user.2", "R")
#
# fx.createService("application.3.input")
# fx.createPermission("application.3.input", "application.user.2", "W")
# fx.createPermission("application.3.input", "application.user.3", "R")
#
# monitor = {
#     'name': 'demo',
#     'startOffset': 0,
#     'feeds': '[{"service":"application.1.input","pattern":"data.txt","status":"Arrived"},{"service":"application.2.input","pattern":"data.txt"},{"service":"application.3.input","pattern":"data.txt"}]',
# }
# fx.createMonitor(monitor)

# s = time.time()
# f = '1kb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
# s = time.time()
# f = '10kb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
# s = time.time()
# f = '100kb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
# s = time.time()
# f = '1mb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
# s = time.time()
# f = '10mb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
# s = time.time()
# f = '100mb.txt'
# fx.upload(f, 'application.1.input')
# print f, ' in ', time.time() - s, 'seconds'
s = time.time()
f = '1gb.txt'
fx.upload(f, 'application.1.input')
print f, ' in ', time.time() - s, 'seconds'
