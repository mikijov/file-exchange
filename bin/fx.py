import argparse
from datetime import datetime, timedelta, tzinfo
import time
import base64
import os.path
import requests
import sys


class FixedOffset(tzinfo):
    """Fixed offset in minutes: `time = utc_time + utc_offset`."""
    def __init__(self, offset):
        self.__offset = timedelta(minutes=offset)
        hours, minutes = divmod(offset, 60)
        # NOTE: the last part is to remind about deprecated POSIX GMT+h timezones
        #  that have the opposite sign in the name;
        #  the corresponding numeric value is not used e.g., no minutes
        self.__name = '<%+03d%02d>%+d' % (hours, minutes, -hours)

    def utcoffset(self, dt=None):
        return self.__offset

    def tzname(self, dt=None):
        return self.__name

    def dst(self, dt=None):
        return timedelta(0)

    def __repr__(self):
        return 'FixedOffset(%d)' % (self.utcoffset().total_seconds() / 60)


def parseDateTime(timestr):
    dt = datetime.strptime(timestr[:23], '%Y-%m-%dT%H:%M:%S.%f')

    offsetStr = timestr[23:]
    offset = int(offsetStr[1:2])*60 + int(offsetStr[3:4])
    if offsetStr[0] == '-':
        offset = -offset
    dt = dt.replace(tzinfo=FixedOffset(offset))
    return dt


PERMISSIONS = ['-', 'L', 'R', 'W', 'A']


class FileExchangeError(RuntimeError):
    # class FileExchangeError(RuntimeError):
    #     def __init__(self, message):
    #         RuntimeError.__init__(self)
    #         self.message = message
    #
    def getMessage(self):
        return str(self)


class FileExchange:
    def __init__(self, url, username, password, verbose=False):
        self.url = url
        self.verbose = verbose
        self.header = {
            'Authorization': 'Basic ' + base64.b64encode(username + ':' + password),
        }

    def listUsers(self, pattern=None):
        s = requests.Session()
        url = self.url + 'users'
        if pattern:
            url = url + '?pattern=' + pattern
        r = s.get(url,
                  headers=self.header)
        if r.status_code != 200:
            raise FileExchangeError('Failed to list users. ({})'.format(r.status_code))
        if self.verbose:
            sys.stderr.write('Users:' + r.text + '\n')

        return r.json()

    def createUser(self, username, password):
        data = {"username": username}
        s = requests.Session()
        r = s.post(self.url + 'users?password=' + password,
                   headers=self.header,
                   json=data)
        if r.status_code != 204:
            raise FileExchangeError('Failed to create user. ({})'.format(r.status_code))

    def changeUserPassword(self, username, password):
        s = requests.Session()
        r = s.put(self.url + 'users/' + username + '/password?password=' + password,
                  headers=self.header)
        if r.status_code != 204:
            raise FileExchangeError('Failed to change password. ({})'.format(r.status_code))

    def deleteUser(self, username):
        s = requests.Session()
        r = s.delete(self.url + 'users/' + username,
                     headers=self.header)
        if r.status_code != 204:
            raise FileExchangeError('Failed to delete user. ({})'.format(r.status_code))

    def listServices(self, pattern=None):
        s = requests.Session()
        url = self.url + 'services'
        if pattern:
            url = url + '?pattern=' + pattern
        r = s.get(url,
                  headers=self.header)
        if r.status_code != 200:
            raise FileExchangeError('Failed to list services. ({})'.format(r.status_code))
        if self.verbose:
            sys.stderr.write('Services:' + r.text + '\n')

        return r.json()

    def createService(self, serviceName):
        data = {"name": serviceName}
        s = requests.Session()
        r = s.post(self.url + 'services',
                   headers=self.header,
                   json=data)
        if r.status_code != 204:
            raise FileExchangeError('Failed to create service. ({})'.format(r.status_code))

    def deleteService(self, service):
        s = requests.Session()
        r = s.delete(self.url + 'services/' + service,
                     headers=self.header)
        if r.status_code != 204:
            raise FileExchangeError('Failed to delete service. ({})'.format(r.status_code))

    def getServicePermissions(self, service):
        s = requests.Session()
        r = s.get(self.url + 'permissions?service=' + service,
                  headers=self.header)
        if r.status_code != 200:
            raise FileExchangeError('Failed to list perrmissions. ({})'.format(r.status_code))
        if self.verbose:
            sys.stderr.write('Permissions:' + r.text + '\n')

        return r.json()

    def createPermission(self, service, username, permission):
        # convert permission into integer if required
        if permission in PERMISSIONS:
            permission = PERMISSIONS.index(permission)

        data = {'service': service, 'username': username, 'permission': permission}
        s = requests.Session()
        r = s.post(self.url + 'permissions',
                   headers=self.header,
                   json=data)
        if r.status_code != 204:
            raise FileExchangeError('Failed to create permission. ({})'.format(r.status_code))

    def deletePermission(self, service, username):
        data = {'service': service, 'username': username}
        s = requests.Session()
        r = s.delete(self.url + 'permissions',
                     headers=self.header,
                     json=data)
        if r.status_code != 204:
            raise FileExchangeError('Failed to delete permission. ({})'.format(r.status_code))

    def listFiles(self, service, pattern=None):
        s = requests.Session()
        url = self.url + 'services/' + service + '/files'
        if pattern:
            url = url + '?pattern=' + pattern
        r = s.get(url,
                  headers=self.header)
        if r.status_code != 200:
            raise FileExchangeError('Failed to list files. ({})'.format(r.status_code))
        if self.verbose:
            sys.stderr.write('Files:' + r.text + '\n')

        files = r.json()
        for file in files:
            file['timeCreated'] = parseDateTime(file['timeCreated'])
        return files

    def upload(self, localFile, remoteFile):
        if remoteFile.count('/') == 1:
            service, remoteFile = remoteFile.split('/', 1)
        elif remoteFile.count('/') > 1:
            raise FileExchangeError('Invalid destination. Format is service/file but multiple separators '/' found.')
        else:
            service = remoteFile
            remoteFile = os.path.basename(localFile)

        with open(localFile, 'rb') as file:
            multipart = {'file': file}
            s = requests.Session()
            r = s.post(self.url + 'services/' + service + '/files/' + remoteFile,
                       headers=self.header,
                       files=multipart)
            if r.status_code != 200:
                raise FileExchangeError('Failed to upload file. ({})'.format(r.status_code))

    def download(self, remoteFile, localFile=None, overwrite=False, timeout=0, retryInterval=10):
        service, remoteFile = remoteFile.split('/')
        if localFile:
            if os.path.isdir(localFile):
                localFile = os.path.join(localFile, remoteFile)
        else:
            localFile = remoteFile

        expiry = datetime.utcnow() + timedelta(seconds=timeout)
        s = requests.Session()
        while True:
            r = s.get(self.url + 'services/' + service + '/files/' + remoteFile,
                      headers=self.header,
                      stream=True)
            if r.status_code != 404 or datetime.utcnow() > expiry:
                break
            time.sleep(retryInterval)

        if r.status_code != 200:
            raise FileExchangeError('Failed to get file. ({})'.format(r.status_code))

        if not overwrite and os.path.exists(localFile):
            raise FileExchangeError('Output file already exists.')
        if localFile:
            with (open(localFile, 'wb')) as file:
                for chunk in r.iter_content(16384):
                    file.write(chunk)
        else:
            return r.raw

    def deleteFile(self, remoteFile):
        service, remoteFile = remoteFile.split('/')
        s = requests.Session()
        r = s.delete(self.url + 'services/' + service + '/files/' + remoteFile,
                     headers=self.header)
        if r.status_code != 204:
            raise FileExchangeError('Failed to delete file. ({})'.format(r.status_code))

    def createMonitor(self, monitor):
        s = requests.Session()
        r = s.post(self.url + 'monitors',
                   headers=self.header,
                   json=monitor)
        if r.status_code != 204:
            raise FileExchangeError('Failed to create monitor. ({})'.format(r.status_code))


def humanMemory(size):
    if size < 100000:
        return '{:5d}'.format(size)
    size /= 1024
    if size < 10000:
        return '{:4d}K'.format(size)
    size /= 1024
    if size < 10000:
        return '{:4d}M'.format(size)
    size /= 1024
    if size < 10000:
        return '{:4d}G'.format(size)
    size /= 1024
    return '{:4d}T'.format(size)


def parseCommandLine():
    parser = argparse.ArgumentParser(
        description='Command line client for FileExchange.'
    )
    parser.add_argument('-u', dest='auth_user', required=True,
                        help='required; username')
    parser.add_argument('-p', dest='auth_pw', required=True,
                        help='required; password')
    parser.add_argument('-v', dest='verbose', action='store_true',
                        help='enable verbose command output for debugging purposes')
    parser.add_argument('-o', dest='overwrite', action='store_true',
                        help='overwrite file if output file already exists')
    parser.add_argument('--url', dest='url',
                        default='http://localhost:8080/file-exchange/api/',
                        help='url to FileExchange REST web service.')

    subparsers = parser.add_subparsers(title=None,
                                       description='Available commands are listed below. Enter command followed by -h for command specific help.',
                                       dest='command',
                                       metavar='command')

    subparsers.add_parser('list-users',
                          help='list users')

    subparser = subparsers.add_parser('create-user',
                                      help='create new user')
    subparser.add_argument('username',
                           help='username for the new user')
    subparser.add_argument('password',
                           help='password for the new user')

    subparser = subparsers.add_parser('delete-user',
                                      help='delete user')
    subparser.add_argument('username',
                           help='user to delete')

    subparser = subparsers.add_parser('change-password',
                                      help='change user password')
    subparser.add_argument('username',
                           help='username whose password to change')
    subparser.add_argument('password',
                           help='new password')

    subparsers.add_parser('list-services',
                          help='list services')

    subparser = subparsers.add_parser('create-service',
                                      help='create new service')
    subparser.add_argument('service',
                           help='service name for the new service')

    subparser = subparsers.add_parser('delete-service',
                                      help='delete service')
    subparser.add_argument('service',
                           help='service name to delete')

    subparser = subparsers.add_parser('list-permissions',
                                      help='list user permissions for particular service')
    subparser.add_argument('service',
                           help='service name for which to retrieve permissions')

    subparser = subparsers.add_parser('create-permission',
                                      help='add/update user permission for particular service')
    subparser.add_argument('service',
                           help='service name for which to create permission')
    subparser.add_argument('username',
                           help='user to which to grant permission')
    subparser.add_argument('permission',
                           help='permission which to grant R/W/A or 1/2/4')

    subparser = subparsers.add_parser('delete-permission',
                                      help='delete user permission for particular service')
    subparser.add_argument('service',
                           help='service name for which to create permission')
    subparser.add_argument('username',
                           help='user to which to grant permission')

    subparser = subparsers.add_parser('list-files',
                                      help='list files')
    subparser.add_argument('service',
                           help='service name for which to retrieve file list')
    subparser.add_argument('pattern',
                           nargs='?',
                           help='optional Java style regular expression to search for files')

    subparser = subparsers.add_parser('get',
                                      help='download file from server')
    subparser.add_argument('remoteFile',
                           help='service and name of the file to retrieve in the "service/fileName" format')
    subparser.add_argument('localFile',
                           nargs='?',
                           help='optional path to local destination file')

    subparser = subparsers.add_parser('put',
                                      help='upload file to server')
    subparser.add_argument('localFile',
                           help='path to the local file to upload')
    subparser.add_argument('destination',
                           nargs='?',
                           help='destination service and optional new name under which to upload the file in service[/new_name] format')

    # subparser = subparsers.add_parser('create-monitor',
    #                                   help='create monitor')
    # subparser.add_argument('monitorName',
    #                        help='name for the new monitor')
    # subparser.add_argument('feeds',
    #                        help='string containing json list of feed definitions')

    return parser.parse_args()


if __name__ == "__main__":
    args = parseCommandLine()

    try:
        fx = FileExchange(args.url,
                          username=args.auth_user,
                          password=args.auth_pw,
                          verbose=args.verbose)

        if args.command == 'list-files':
            for file in fx.listFiles(args.service, args.pattern):
                print '{:%Y-%m-%d %H:%M:%S} {} {}'.format(file['timeCreated'],
                                                          humanMemory(file['fileSize']),
                                                          file['filename'])
        elif args.command == 'list-users':
            for user in fx.listUsers():
                print '{}'.format(user['username'])
        elif args.command == 'create-user':
            fx.createUser(args.username, args.password)
        elif args.command == 'delete-user':
            fx.deleteUser(args.username)
        elif args.command == 'change-password':
            fx.changeUserPassword(args.username, args.password)
        elif args.command == 'list-services':
            for service in fx.listServices():
                print '{}'.format(service['name'])
        elif args.command == 'create-service':
            fx.createService(args.service)
        elif args.command == 'delete-service':
            fx.deleteService(args.service)
        elif args.command == 'list-permissions':
            for permission in fx.getServicePermissions(args.service):
                print '{} {}'.format(PERMISSIONS[permission['permission']],
                                     permission['username'])
        elif args.command == 'create-permission':
            fx.createPermission(args.service, args.username, args.permission)
        elif args.command == 'delete-permission':
            fx.deletePermission(args.service, args.username)
        elif args.command == 'put':
            fx.upload(args.localFile, args.destination)
        elif args.command == 'get':
            fx.download(args.remoteFile, args.localFile, args.overwrite)
        else:
            sys.stderr.write('Unexpexted error. Undetected bad command.\n')
            sys.exit(1)
    except FileExchangeError as x:
        sys.stderr.write(x.getMessage() + '\n')
        sys.exit(1)
