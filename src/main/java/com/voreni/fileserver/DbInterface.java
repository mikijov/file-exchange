package com.voreni.fileserver;

import java.sql.SQLException;
import java.util.List;

import org.joda.time.DateTime;

public abstract class DbInterface
    implements AutoCloseable
{
    // permissions
    public static final int NONE = 0;
    public static final int LIST = 1;
    public static final int READ = 2;
    public static final int WRITE = 3;
    public static final int ADMIN = 4;

    public abstract void createSchema() throws SQLException;

    public abstract DbIterator<User> getUsers(String pattern) throws SQLException;
    public abstract User getUser(long id) throws SQLException;
    public abstract User getUser(String username) throws SQLException;
    public abstract long createUser(User user) throws SQLException;
    public abstract void updateUser(String username, User user) throws SQLException;
    public abstract void deleteUser(long id) throws SQLException;
    public abstract void deleteUser(String username) throws SQLException;

    public abstract DbIterator<Service> getServices(String pattern) throws SQLException;
    public abstract long createService(Service service) throws SQLException;
    public abstract void updateService(String serviceName, Service service) throws SQLException;
    public abstract void deleteService(String serviceName) throws SQLException;

    public abstract DbIterator<Permission> getPermissions(String service, String username) throws SQLException;
    public abstract int getPermission(String serviceName, long userId) throws SQLException;
    public abstract void createPermission(Permission permission) throws SQLException;
    public abstract void deletePermission(Permission permission) throws SQLException;

    public abstract DbIterator<File> getFiles(String serviceName, String pattern,
        DateTime fromTime, DateTime toTime) throws SQLException;
    public abstract File getFile(String serviceName, String filename) throws SQLException;
    public abstract void createFile(String serviceName, File file) throws SQLException;
    public abstract void deleteFile(String serviceName, String filename) throws SQLException;

    public abstract DbIterator<Monitor> getMonitors(String pattern) throws SQLException;
    public abstract Monitor getMonitor(String monitorName) throws SQLException;
    public abstract long createMonitor(Monitor monitor) throws SQLException;
    public abstract void updateMonitor(String monitorName, Monitor monitor) throws SQLException;
    public abstract void deleteMonitor(String monitorName) throws SQLException;

    public abstract DbIterator<Audit.Entry> getAudit() throws SQLException;
    public abstract void saveAudit(List<Audit.Entry> entries) throws SQLException;

    public abstract boolean isDuplicateError(SQLException e);
    public abstract boolean isNotFoundError(SQLException e);
}
