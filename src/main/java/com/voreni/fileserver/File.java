package com.voreni.fileserver;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.voreni.fileserver.api.MyJacksonFeature;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import org.joda.time.DateTime;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public class File {
    @JsonProperty
    private String filename;
    @JsonProperty
    private long fileSize;
    @JsonProperty
    @JsonFormat(pattern=MyJacksonFeature.ISO8601_FORMAT)
    private DateTime timeCreated;

    @JsonProperty
    private String sha1;
    private String compressedFilename;
    @JsonProperty
    private long compressedFileSize;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long size) {
        this.fileSize = size;
    }

    public DateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(DateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }
    public long getCompressedFileSize() {
        return compressedFileSize;
    }

    public void setCompressedFileSize(long size) {
        this.compressedFileSize = size;
    }

    public String getCompressedFilename() {
        return compressedFilename;
    }

    public void setCompressedFilename(String filename) {
        this.compressedFilename = filename;
    }

    @Override
    @SuppressFBWarnings
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof File)) {
            return false;
        }
        File otherFile = (File)other;
        return (filename == otherFile.getFilename() || filename.equals(otherFile.getFilename()))
                        && (compressedFilename == otherFile.getCompressedFilename() || compressedFilename.equals(otherFile.getCompressedFilename()))
                        && (timeCreated == otherFile.getTimeCreated() || timeCreated.equals(otherFile.getTimeCreated()))
                        && (sha1 == otherFile.getSha1() || filename.equals(otherFile.getSha1()));
    }
}
