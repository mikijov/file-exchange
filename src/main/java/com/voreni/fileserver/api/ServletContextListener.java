package com.voreni.fileserver.api;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import com.voreni.Log;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbSqlite;
import com.voreni.fileserver.Stats;

@WebListener
public class ServletContextListener
    implements javax.servlet.ServletContextListener
{
	private final static Logger LOGGER = Logger.getLogger(ServletContextListener.class.getName());
	private static String HOME = null;

	public static String getHome() {
	    return HOME;
	}

    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        try {
            Log.initialize("file-exchange");

            HOME = System.getenv("FILE_EXCHANGE_HOME");
            if (HOME == null) {
                LOGGER.severe("FILE_EXCHANGE_HOME is not set. Aborting.");
                throw new RuntimeException("FILE_EXCHANGE_HOME is not set. Aborting.");
            }
            LOGGER.config("FILE_EXCHANGE_HOME=" + HOME);
            java.io.File file = new java.io.File(HOME, "file-exchange.sqlite");
            if (file.exists()) {
                LOGGER.config("Initializing FileExchange servlet.");
                DbSqlite sqlite = new DbSqlite(file.getAbsolutePath(), false);
                sqlite.initializeStats();
                Db.setSingleton(sqlite);
            }
            else {
                LOGGER.config("Creating new database.");
                DbSqlite sqlite = new DbSqlite(file.getAbsolutePath(), true);
                sqlite.createSchema();
                Db.setSingleton(sqlite);
            }

            Stats.initialize();
            Audit.initialize();
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Unable to load settings.", e);
            throw new RuntimeException("Unable to load settings.", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        LOGGER.config("Unloading FileExchange servlet.");

        Audit.done();
        Stats.done();
    }

}