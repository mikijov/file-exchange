(function() {

  "use strict";

  angular.module("fxUserDetails", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/users/:username", {
      templateUrl: "userDetails.html",
      controller: "userDetailsCtrl"
    });
  }])

  .controller("userDetailsCtrl", [
    "$scope", "$log", "fxApi", "$routeParams", "$location",
    function($scope, $log, fxApi, $routeParams, $location) {
    $scope.user = {};
    $scope.permissions = [];
    $scope.$parent.viewName = "users";

    $scope.onSave = function() {
      var promise;
      if ($scope.isNew) {
        promise = fxApi.createUser($scope.user);
      }
      else {
        promise = fxApi.updateUser($routeParams.username, $scope.user);
      }
      promise.then(
        function(response) {
          $log.info("User saved.");
          if ($scope.isNew) {
            $log.info("/users/" + $scope.user.username);
            $location.url("/users/" + $scope.user.username);
          }
          else {
            $scope.editing = false;
            $scope.errorMessage = null;
          }
        },
        function(response) {
          $log.info("Problem saving user.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onDelete = function() {
      fxApi.deleteUser($scope.user.username)
      .then(
        function() {
          $location.url("/users");
        },
        function(response) {
          $log.info("Problem deleting user.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onCancel = function() {
      $location.url("/users");
    };

    $scope.onAddPermission = function() {
      $scope.newPermission = {
        username: $scope.user.username,
        service: "",
        permission: "1",
      };
    };

    $scope.onEditPermission = function(permission) {
      $scope.currentPermission = permission;
      $scope.savedPermission = permission.permission;
    };

    $scope.onSaveEdit = function(permission) {
      fxApi.createPermission(permission)
      .then(
        function() {
          $scope.currentPermission = null;
          $scope.permissionErrorMessage = null;
        },
        function(response) {
          $log.info("Problem saving permission.");
          $scope.permissionErrorMessage = response.statusText;
        }
      );
    };

    $scope.onCancelEdit = function(permission) {
      permission.permission = $scope.savedPermission;
      $scope.currentPermission = null;
    };

    $scope.onDeletePermission = function(permission) {
      fxApi.deletePermission(permission)
      .then(
        function() {
          $scope.currentPermission = null;
          $scope.permissionErrorMessage = null;
          var index = $scope.permissions.indexOf(permission);
          $scope.permissions.splice(index, 1);
        },
        function(response) {
          $log.info("Problem deleting permission.");
          $scope.permissionErrorMessage = response.statusText;
        }
      );
    };

    $scope.onSaveNewPermission = function() {
      fxApi.createPermission($scope.newPermission)
      .then(
        function() {
          $scope.permissions.push($scope.newPermission);
          $scope.newPermission = null;
          $scope.permissionErrorMessage = null;
        },
        function(response) {
          $log.info("Problem saving permission.");
          $scope.permissionErrorMessage = response.statusText;
        }
      );
    };

    $scope.onCancelNewPermission = function() {
      $scope.newPermission = null;
    };

    $scope.getPermissionClass = function(permission) {
      if (permission == 1) {
        return "bg-light-blue";
      }
      else if (permission == 2) {
        return "bg-green";
      }
      else if (permission == 3) {
        return "bg-yellow";
      }
      else if (permission == 4) {
        return "bg-red";
      }
      else {
        return "";
      }
    };

    $scope.getPermissionText = function(permission) {
      if (permission == 1) {
        return "List";
      }
      else if (permission == 2) {
        return "Read";
      }
      else if (permission == 3) {
        return "Write";
      }
      else if (permission == 4) {
        return "Admin";
      }
      else {
        return "";
      }
    };

    $scope.load = function() {
      fxApi.getUser($routeParams.username)
      .then(
        function(user) {
          $log.info("User:", angular.toJson(user));
          $scope.user = user;
        },
        function(response) {
          $log.error("Failed to get user.", response.status, response.statusText);
        }
      );

      fxApi.getUserPermissions($routeParams.username)
      .then(
        function(permissions) {
          $log.info("Permissions:", angular.toJson(permissions));
          $scope.permissions = permissions;
        },
        function(response) {
          $log.error("Failed to get permissions.", response.status, response.statusText);
        }
      );
    };

    $scope.isNew = $routeParams.username == "!new";
    if ($scope.isNew) {
      $scope.user = {
        username:"",
        isSysAdmin:false,
      };
      $scope.editing = true;
    }
    else {
      $scope.load();
    }

  }]);

})();
