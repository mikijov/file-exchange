package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.DatatypeConverter;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.File;
import com.voreni.fileserver.Stats;
import com.voreni.fileserver.User;

@Path("services/{service}/files")
public class FilesWebService {
	private final static Logger LOGGER = Logger.getLogger(FilesWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listFiles(
        @PathParam("service") String serviceName,
        @QueryParam("fromTime") String fromTime,
        @QueryParam("toTime") String toTime,
        @QueryParam("pattern") final String pattern
        )
    {
        LOGGER.fine("listFiles()");

        User user = (User)request.getProperty(AuthFilter.USER);
        Auth.checkPermission(user, serviceName, Auth.Authorization.LIST);

        DateTime fromTime2 = null;
        DateTime toTime2 = null;
        try {
            if (fromTime != null) {
                fromTime2 = MyJacksonFeature.isoFormatter.parseDateTime(fromTime);
            }
            if (fromTime != null) {
                toTime2 = MyJacksonFeature.isoFormatter.parseDateTime(toTime);
            }
        }
        catch (IllegalArgumentException e) {
            throw new WebApplicationException(
                "Supplied date is not in ISO8601 format.",
                Response.Status.BAD_REQUEST
                );
        }

        final Pattern regex = Ws.compilePattern(pattern);

        try {
            final DbIterator<File> iter = Db.getFiles(serviceName, null, fromTime2, toTime2);
            // TODO: User ObjectMapper from MyJacksonFeature
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            File file = iter.next();
                            if (regex == null || regex.matcher(file.getFilename()).find()) {
                                jg.writeObject(file);
                            }
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listFiles(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            LOGGER.fine("listFiles(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    // actual upload file worker called by the two end points
    public Response uploadFile(
        String serviceName,
        String filename,
        InputStream is
        )
    {
        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, serviceName, Auth.Authorization.WRITE);

        try {
            java.nio.file.Path dataDir = FileSystems.getDefault()
                            .getPath(ServletContextListener.getHome()).toAbsolutePath();

            java.nio.file.Path path = Files.createTempFile(dataDir.resolve("tmp"), "file-server-", null).toAbsolutePath();
            LOGGER.finer("Saving to temp file " + path.toString());

            MessageDigest checksum = MessageDigest.getInstance("SHA-1");
            long originalFileSize = 0;

            try (
                            OutputStream fileStream = Files.newOutputStream(path);
                            OutputStream os = new GZIPOutputStream(fileStream)
                    )
            {
                Stats.startUpload();
                try {
                    byte[] buffer = new byte[16384];
                    while (true) {
                        int read = is.read(buffer);
                        if (read == -1) {
                            break;
                        }
                        originalFileSize += read;
                        checksum.update(buffer, 0, read);
                        os.write(buffer, 0, read);
                        Stats.addUploadSize(read);
                    }
                }
                finally {
                    Stats.stopUpload();
                }
            }

            byte[] checksumBytes = checksum.digest();
            String checksumString = DatatypeConverter.printHexBinary(checksumBytes);

            java.nio.file.Path targetPath = dataDir
                            .resolve(checksumString.substring(0, 2))
                            .resolve(checksumString.substring(2, 4))
                            .resolve(checksumString + ".gz");
            Files.createDirectories(targetPath.getParent());

            if (Files.exists(targetPath)) {
                LOGGER.warning(checksumString + ".gz already exists. Assuming identical content.");
                // target file exists, assume identical content
                Files.delete(path);
            }
            else {
                Files.move(path, targetPath);
            }

            File file = new File();
            file.setFilename(filename);
            file.setCompressedFilename(targetPath.getFileName().toString());
            file.setSha1(checksumString);
            file.setTimeCreated(DateTime.now());
            file.setFileSize(originalFileSize);
            file.setCompressedFileSize(Files.size(targetPath));

            Db.createFile(serviceName, file);

            Audit.log(user, ip, Audit.FILE_CREATE, serviceName, filename);

            LOGGER.fine("uploadFile(" + serviceName + ", " + filename + ") saved " + targetPath.toString());

            return Response.ok(file).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (IOException | NoSuchAlgorithmException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @POST
    @Path("{filename}")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFileDirect(
        @PathParam("service") String serviceName,
        @PathParam("filename") String filename,
        InputStream is
        )
    {
        LOGGER.fine("uploadFileDirect()");
        return uploadFile(serviceName, filename, is);
    }

    @POST
    @Path("{filename}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFileMultiPart(
        @PathParam("service") String serviceName,
        @PathParam("filename") String filename,
//        @QueryParam("copyFrom") String otherServiceName,
        @FormDataParam("file") InputStream is
        )
    {
        LOGGER.fine("uploadFileMultiPart()");
        return uploadFile(serviceName, filename, is);
    }

    @DELETE
    @Path("{filename}")
    public void deleteFile(
        @PathParam("service") String serviceName,
        @PathParam("filename") String filename
        )
    {
        LOGGER.fine("deleteFile()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, serviceName, Auth.Authorization.ADMIN);

        try {
            Db.deleteFile(serviceName, filename);

            Audit.log(user, ip, Audit.FILE_DELETE, serviceName, filename);

            LOGGER.fine("deleteFile(" + serviceName + ", " + filename + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @GET
    @Path("{filename}")
    public Response downloadFile(
        @PathParam("service") final String serviceName,
        @PathParam("filename") final String filename
        )
    {
        LOGGER.fine("downloadFile()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, serviceName, Auth.Authorization.READ);

        try {
            File file = Db.getFile(serviceName, filename);

            java.nio.file.Path dataDir = FileSystems.getDefault()
                            .getPath(ServletContextListener.getHome()).toAbsolutePath();

            String actualFilename = file.getCompressedFilename();
            final java.nio.file.Path path = dataDir
                            .resolve(actualFilename.substring(0, 2))
                            .resolve(actualFilename.substring(2, 4))
                            .resolve(actualFilename);

            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) {
                    try {
                        try (InputStream is = new GZIPInputStream(Files.newInputStream(path))) {
                            Stats.startDownload();
                            try {
                                byte[] buffer = new byte[16384];
                                while (true) {
                                    int read = is.read(buffer);
                                    if (read == -1) {
                                        break;
                                    }
                                    os.write(buffer, 0, read);
                                    Stats.addDownloadSize(read);
                                }
                            }
                            finally {
                                Stats.stopDownload();
                            }
                        }
                        os.flush();
                        LOGGER.fine("downloadFile(" + serviceName + ", " + filename + ") finished");
                    }
                    catch (IOException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                }
            };

            LOGGER.fine("downloadFile(" + serviceName + ", " + filename + ") starting");

            Audit.log(user, ip, Audit.FILE_READ, serviceName, filename);

            return Response.ok(stream, MediaType.APPLICATION_OCTET_STREAM).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }
}
