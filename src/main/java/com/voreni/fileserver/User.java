package com.voreni.fileserver;

import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.voreni.security.Security;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public class User {
    private long id;
    @JsonProperty
    private String username;
    private String salt;
    private String password;
    @JsonProperty
    private boolean isSysAdmin = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public byte[] generateSalt() throws NoSuchAlgorithmException {
        byte[] salt = Security.generateSalt(8);
        this.salt = DatatypeConverter.printHexBinary(salt);
        return salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSysAdmin() {
        return isSysAdmin;
    }

    public void setSysAdmin(boolean isSysAdmin) {
        this.isSysAdmin = isSysAdmin;
    }

    @Override
    @SuppressFBWarnings
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        User user = (User)other;
        return id == user.getId()
                && (username == user.getUsername() || username.equals(user.getUsername()))
                && (salt == user.getSalt() || salt.equals(user.getSalt()))
                && (password == user.getPassword() || password.equals(user.getPassword()))
                && isSysAdmin == user.isSysAdmin();
    }
}
