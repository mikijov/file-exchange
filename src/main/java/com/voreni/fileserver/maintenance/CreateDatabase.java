package com.voreni.fileserver.maintenance;

import java.io.File;

import com.voreni.fileserver.DbSqlite;

public class CreateDatabase {

    public static void main(String[] args) throws Exception {
        String path = "/Users/miki/tmp/file-exchange/file-exchange.sqlite";
        new File(path).delete();
        try (DbSqlite db = new DbSqlite(path, true)) {
            db.createSchema();
        }
        System.out.println("Done.");
    }

}
