(function() {

  "use strict";

  angular.module("fxDashboard", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/dashboard", {
      templateUrl: "dashboard.html",
      controller: "dashboardCtrl"
    });
  }])

  .controller("dashboardCtrl", ["$scope", "$log", "fxApi", "$timeout",
              function($scope, $log, fxApi, $timeout) 
  {
    $scope.$parent.viewName = "dashboard";
    $scope.stats = {};

    $scope.createChart = function(granularity) {
      $scope.chartGranularity = granularity;
      var data = {
        labels: [],
        datasets: [
          {
            label: "Disk Usage",
            fillColor: "#d2d6de",
            strokeColor: "#d2d6de",
            pointColor: "#ffffff",
            pointStrokeColor: "#ff851b",
            pointHighlightFill: "#ff851b",
            pointHighlightStroke: "#ff851b",
            data: []
          },
          {
            label: "Download",
            fillColor: "#00a65a",
            strokeColor: "#00a65a",
            pointColor: "#ffffff",
            pointStrokeColor: "#ff851b",
            pointHighlightFill: "#ff851b",
            pointHighlightStroke: "#ff851b",
            data: []
          },
          {
            label: "Upload",
            fillColor: "#3c8dbc",
            strokeColor: "#3c8dbc",
            pointColor: "#ffffff",
            pointStrokeColor: "#ff851b",
            pointHighlightFill: "#ff851b",
            pointHighlightStroke: "#ff851b",
            data: []
          },
        ]
      };
      data.labels = [];
      switch (granularity) {
        case 0:
          var sampleCount = 60;
          break;
        case 1:
          var sampleCount = 60;
          break;
        case 2:
          var sampleCount = 24;
          break;
        case 3:
          var sampleCount = 30;
          break;
        default:
          $log.error("Unsupported chart granularity.");
          return;
      }
      for (var i = 0; i < sampleCount; ++i) {
        data.labels.push(((i-sampleCount-0) % 5 == 0) ? i-sampleCount : "");
        data.datasets[0].data.push(0);
        data.datasets[1].data.push(0);
        data.datasets[2].data.push(0);
      }
      
      var canvas = $("#barChart").get(0).getContext("2d");
      var chartConstuctor = new Chart(canvas);
      var options = {
        animation: false,
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: false,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 3,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        // pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: false,
        //Boolean - If there is a stroke on each bar
        // barShowStroke: true,
        //Number - Pixel width of the bar stroke
        // barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        // barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        // barDatasetSpacing: 0,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: false,

        // Boolean - Determines whether to draw tooltips on the canvas or not
        showTooltips: false,
        // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
        customTooltips: false,
        // Array - Array of string names to attach tooltip events
        tooltipEvents: ["mousemove", "touchstart", "touchmove"],
        // String - Tooltip background colour
        tooltipFillColor: "rgba(0,0,0,0.7)",
        // String - Tooltip label font declaration for the scale label
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        // Number - Tooltip label font size in pixels
        tooltipFontSize: 11,
        // String - Tooltip font weight style
        tooltipFontStyle: "normal",
        // String - Tooltip label font colour
        tooltipFontColor: "#fff",
        // String - Tooltip title font declaration for the scale label
        tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        // Number - Tooltip title font size in pixels
        tooltipTitleFontSize: 12,
        // String - Tooltip title font weight style
        tooltipTitleFontStyle: "bold",
        // String - Tooltip title font colour
        tooltipTitleFontColor: "#fff",
        // Number - pixel width of padding around tooltip text
        tooltipYPadding: 4,
        // Number - pixel width of padding around tooltip text
        tooltipXPadding: 4,
        // Number - Size of the caret on the tooltip
        tooltipCaretSize: 8,
        // Number - Pixel radius of the tooltip border
        tooltipCornerRadius: 4,
        // Number - Pixel offset from point x to tooltip edge
        tooltipXOffset: 10,
        // String - Template string for single tooltips
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
        // String - Template string for multiple tooltips
        multiTooltipTemplate: "<%= value %> GB",
      };

      $scope.chart = chartConstuctor.Line(data, options);
    };

    $scope.changeGranularity = function(granularity) {
      $scope.createChart(granularity);
      $scope.updateChart();
    };

    $scope.updateChart = function() {
      if (!$scope.stats.apiCalls) {
        return;
      }
      var granularity = $scope.chartGranularity;
      var i = 0;
      for (var val of $scope.stats.apiCalls.ranges[granularity].samples) {
        $scope.chart.datasets[0].points[i++].value = val;
      }
      i = 0;
      for (var val of $scope.stats.uploads.ranges[granularity].samples) {
        $scope.chart.datasets[1].points[i++].value = val;
      }
      i = 0;
      for (var val of $scope.stats.downloads.ranges[granularity].samples) {
        $scope.chart.datasets[2].points[i++].value = val;
      }
      $scope.chart.update();
    };

    $scope.refreshStats = function() {
      fxApi.getStats()
      .then(
        function(stats) {
          $log.info("Stats:", angular.toJson(stats));
          $scope.stats = stats;
          $scope.updateChart();
          $scope.timer = $timeout($scope.refreshStats, 1000);
        },
        function(response) {
          $log.error("Failed to get stats.", response.status, response.statusText);
          $scope.timer = $timeout($scope.refreshStats, 10000);
        }
      );
    };

    // When the DOM element is removed from the page,
    // AngularJS will trigger the $destroy event on
    // the scope. This gives us a chance to cancel any
    // pending timer that we may have.
    $scope.$on(
      "$destroy",
      function(event) {
        $timeout.cancel($scope.timer);
      }
    );

    $scope.changeGranularity(0);
    $scope.refreshStats();
  }]);

})();
