(function() {

  "use strict";

  angular.module("fxFiles", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/services/:service/files", {
      templateUrl: "files.html",
      controller: "filesCtrl"
    });
  }])

  .controller("filesCtrl", [
    "$scope", "$log", "fxApi", "$location", "$routeParams", "$timeout",
    function($scope, $log, fxApi, $location, $routeParams, $timeout) 
  {
    $scope.$parent.viewName = "files";
    $scope.files = [];
    $scope.filter = "";

    $scope.onDelete = function(file) {
      fxApi.deleteFile($routeParams.service, file.filename)
      .then(
        function() {
          $scope.errorMessage = null;
          var index = $scope.files.indexOf(file);
          $scope.files.splice(index, 1);
        },
        function(response) {
          $log.info("Problem deleting file.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onFilter = function() {
      $timeout.cancel($scope.filterTimer);
      $scope.filterTimer = $timeout($scope.refresh, 500);
    };

    $scope.getFileUri = function(file) {
      return fxApi.getFileUri($routeParams.service, file.filename);
    };

    $scope.refresh = function() {
      fxApi.getFiles($routeParams.service, $scope.filter)
      .then(
        function(files) {
          $log.info("Files:", angular.toJson(files));
          $scope.files = files;
        },
        function(response) {
          $log.error("Failed to get files.", response.status, response.statusText);
        }
      );
    };

    $scope.refresh();
  }]);

})();
