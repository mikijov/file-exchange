package com.voreni.fileserver.maintenance;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.xml.bind.DatatypeConverter;

import com.voreni.security.Security;

public class HashPassword {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String password = args.length > 0 ? args[0] : "password";
        System.out.println("Password: " + password);

        byte[] salt = Security.generateSalt(8);
        System.out.println("Salt: " + DatatypeConverter.printHexBinary(salt));

        byte[] hashedPassword = Security.hashPassword(password, salt);
        System.out.println("Hashed Password: " + DatatypeConverter.printHexBinary(hashedPassword));
    }

}
