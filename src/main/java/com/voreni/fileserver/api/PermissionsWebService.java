package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.Permission;
import com.voreni.fileserver.User;

@Path("permissions")
public class PermissionsWebService {
	private final static Logger LOGGER = Logger.getLogger(PermissionsWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listPermissions(
        @QueryParam("service") final String service,
        @QueryParam("username") final String username
        )
    {
        LOGGER.fine("listPermissions()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.AUTHENTICATED);

        try {
            final DbIterator<Permission> iter = Db.getPermissions(service, username);
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            Permission permission = iter.next();
                            jg.writeObject(permission);
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listPermissions(" + service + "," + username + ") Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            Audit.log(user, ip, Audit.PERMISSION_LIST, service, username);

            LOGGER.fine("listPermissions(" + service + "," + username + ") OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createPermission(
        Permission permission
        )
    {
        LOGGER.fine("createPermission()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, permission.getService(), Auth.Authorization.ADMIN);

        try {
            Db.createPermission(permission);

            Audit.log(user, ip, Audit.PERMISSION_CREATE,
                permission.getService(), permission.getUsername(), permission.getPermission());

            LOGGER.fine("createPermission(" + permission.getService() + "," + permission.getUsername() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @DELETE
    public void deletePermission(
        Permission permission
        )
    {
        LOGGER.fine("deletePermission()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, permission.getService(), Auth.Authorization.ADMIN);

        try {
            Db.deletePermission(permission);

            Audit.log(user, ip, Audit.PERMISSION_DELETE, permission.getService(), permission.getUsername());

            LOGGER.fine("deletePermission(" + permission.getService() + "," + permission.getUsername() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }
}
