package com.voreni.fileserver;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public final class Stats extends TimerTask {
    private final static Logger LOGGER = Logger.getLogger(Stats.class.getName());

    private static final int DAYS = 30;
    @JsonProperty
    private StatsGroup apiCalls = new StatsGroup(DAYS);
    @JsonProperty
    private StatsGroup uploads = new StatsGroup(DAYS);
    @JsonProperty
    private StatsGroup uploadsSize = new StatsGroup(DAYS);
    @JsonProperty
    private StatsGroup downloads = new StatsGroup(DAYS);
    @JsonProperty
    private StatsGroup downloadsSize = new StatsGroup(DAYS);
    @JsonProperty
    private long users = 0;
    @JsonProperty
    private long services = 0;
    @JsonProperty
    private long files = 0;
    @JsonProperty
    private long filesSize = 0;
    @JsonProperty
    private long totalUploads = 0;
    @JsonProperty
    private long totalUploadsSize = 0;
    @JsonProperty
    private long totalDownloads = 0;
    @JsonProperty
    private long totalDownloadsSize = 0;

    @JsonProperty
    private AtomicLong currentUploads = new AtomicLong();
    private AtomicLong currentUploadChunk = new AtomicLong();
    @JsonProperty
    private long currentUploadSpeed = 0;
    @JsonProperty
    private AtomicLong currentDownloads = new AtomicLong();
    private AtomicLong currentDownloadChunk = new AtomicLong();
    @JsonProperty
    private long currentDownloadSpeed = 0;

    private static boolean quit = false;

    public long getUploads() {
        return currentUploads.get();
    }

    public long getDownloads() {
        return currentDownloads.get();
    }

    private static final Stats singleton = new Stats();
    private static final Timer timer = new Timer("Stats Runner", true);

    public static void initialize() {
        timer.scheduleAtFixedRate(singleton, 1000, 1000);
    }

    public static void done() {
        quit = true;
        try {
            synchronized (Stats.class) {
                Stats.class.wait(2000);
            }
        }
        catch (InterruptedException e) {
            LOGGER.warning("Did not properly synchrinize while waiting for Stats thread to finish. A memory leak is possible.");
        }
    }

    public static Stats getSingleton() {
        return singleton;
    }

    public static void incApiCall() {
        singleton.apiCalls.add(1);
    }

    public static void startUpload() {
        singleton.uploads.add(1);
        singleton.currentUploads.incrementAndGet();
    }

    public static void stopUpload() {
        singleton.currentUploads.decrementAndGet();
    }

    public static void startDownload() {
        singleton.downloads.add(1);
        singleton.currentDownloads.incrementAndGet();
    }

    public static void stopDownload() {
        singleton.currentDownloads.decrementAndGet();
    }

    public static void addUploadSize(long size) {
        singleton.uploadsSize.add(size);
        singleton.currentUploadChunk.addAndGet(size);
    }

    public static void addDownloadSize(long size) {
        singleton.downloadsSize.add(size);
        singleton.currentDownloadChunk.addAndGet(size);
    }

    public static void setUsers(long count) {
        singleton.users = count;
    }

    public static void setServices(long count) {
        singleton.services = count;
    }

    public static void setFiles(long count) {
        singleton.files = count;
    }

    public static void setFilesSize(long size) {
        singleton.filesSize = size;
    }

    public static void setUploads(long count) {
        singleton.totalUploads = count;
    }

    public static void setUploadsSize(long size) {
        singleton.totalUploadsSize = size;
    }

    public static void setDownloads(long count) {
        singleton.totalDownloads = count;
    }

    public static void setDownloadsSize(long size) {
        singleton.totalDownloadsSize = size;
    }

    @Override
    public void run() {
        if (quit) {
            timer.cancel();
            synchronized (Stats.class) {
                Stats.class.notifyAll();
            }
            return;
        }

        apiCalls.push();
        uploads.push();
        uploadsSize.push();
        downloads.push();
        downloadsSize.push();
        currentUploadSpeed = currentUploadChunk.getAndSet(0);
        currentDownloadSpeed = currentDownloadChunk.getAndSet(0);
    }
}
