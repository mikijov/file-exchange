import argparse
import time
from fx import FileExchange

URL = 'http://localhost:8080/file-exchange/api/'

# parse command line
parser = argparse.ArgumentParser(description='Run demo application.')
parser.add_argument('integer', metavar='N', type=int, nargs=1,
                    help='application number')
parser.add_argument('--stop', action='store_true',
                    help='this is last app, so no uploads')
args = parser.parse_args()
appNumber = args.integer[0]

# run demo application
print "Application ", appNumber, " starting."
fx = FileExchange(URL, 'application.user.' + str(appNumber), 'password')

print "Waiting for data.txt..."
fx.download("application." + str(appNumber) + ".input/data.txt",
            "data." + str(appNumber) + ".txt",
            overwrite=True, timeout=60, retryInterval=1)
print "Got it."

if not args.stop:
    print "Processing..."
    time.sleep(3)
    print "Uploading results.txt to application ", appNumber+1
    fx.upload("data." + str(appNumber) + ".txt",
              "application." + str(appNumber+1) + ".input/data.txt")

print "Done."
