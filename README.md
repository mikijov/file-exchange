# file-exchange 

## Backlog

### Required for Demo

+ switch to AuthFilter and eliminate sessions
+ delete non-funcioning breadcrumbs from the UI
+ rename originalFilename, Size etc. to just filename, and rename size to
  compressedSize
+ add size (KB,GB etc) formatting in the UI

+ CRUD for service monitoring objects
+ ui to create new/edit service monitoring configuration
+ create service monitoring ui

+ highlight current view in the sidebar
+ change checksum from blob to string in the DB

+ ability to add permission in the UI
+ ability to add service in the UI
+ ability to add user in the UI
+ ability to delete permissin in the UI
+ ability to delete file in the UI
+ ability to delete service in the UI
+ ability to delete user in the UI

+ implement UI filtering on users/services/monitors/files

+ load total DB stats on startup

### Required for First Deployment

+ create audit log
+ create audit log UI
- pagination for audit log

+ full review of REST web services, compliance to CRUD
- create load testing scripts
- investigate if service delete should cascade the delete to files, or is it
  satisfactory that the files are unreachable without service

- investigate Json formatting for jodatime.Duration
- hashCode for POJOs
+ use ObjectMapper singleton; it is thread safe and heavy to construct
- string concatenation -> StringBuilder

+ UI: add validation to user editing
- UI: add validation to service editing
- UI: add validation to monitor editing
- investigate wrapping parsing in UI with try/catch to survive unexpected data

- make the UI work for non-admin accounts
- hide non-admin UI elements for normal users
- hide editing features for unauthorized users

- add "last login date" to user

- cache credentials for faster authentication
- Calin suggests "servlet filter" to separate authentication from the endpoint

- investigate vm image building as part of the build

- update total DB stats while running
- add CPU and diskspace into stats
- ability to add file in the UI

- complete documentation

### Next Version

- investigate JPA and Hibernate to simplify code
- create breadcrumbs in the ui
- per user timezone date formatting in the UI

### Not yet prioritized

- password field in the UI should be hinted to disable browser changing
  capitalization or auto correcting spelling
- add picture to user account

- recreate DB from audit log

- list unused files
- list oldest files
- ability to purge deleted files/services/users
- ability to specify how old should purged files/services/users be
- ability to verify file integrity (sha-1)
- add loading screen, and investigate to keep it on while authentication is
  ongoing

## Design

### Principles

- standard REST api
- standard Basic HTTP Authentication
- always HTTPS
- all dates in full ISO 8601, "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

### API

#### User

{
  username: "string",
  isSysAdmin: 0|1|true|false,
}

##### List

GET: /api/users[&pattern=regular-expression]
Response: [ <user>, ... ]
Privilege: SYSADMIN
Get JSON list of all system users.

##### CRUD

POST: /api/users?password=PPP
Request: <user>
Privilege: SYSADMIN
Create user with given password.

GET: /api/users/{username}
Response: <user>
Privilege: SELF
Retrieve user.

PUT: /api/users/{username}
Request: <user>
Privilege: SYSADMIN
Update user. If changing username, URL username must reference old username, and
request should specify new username.

DELETE: /api/users/{username}
Privilege: SYSADMIN
Delete user.

##### Change Password

PUT: /api/users/{username}/password?password=PPP
Privilege: SELF
Change user password.


#### Service

{
  name: "string",
}

##### List

GET: /api/services[&pattern={regular-expression}]
Privilege: AUTHENTICATED
Response: [ <service>, ... ]
Get list of all system users.

##### CRUD

POST: /api/services
Request: <service>
Privilege: SYSADMIN
Create service.

PUT: /api/service/{serviceName}
Request: <service>
Privilege: SYSADMIN
Update service. Note: URL serviceName must reference old name. Request can 
specify new name.

DELETE: /api/service/{serviceName}
Privilege: ADMIN
Delete service id. Associated files will become inaccessible.


#### Permissions

{
  service: "string",
  username: "string",
  privileges:"admin|write|read|list",
}

##### List

GET: /api/permissions?(user={username}|service={serviceName})
Response: [ <permission>, ... ]
Privilege: LIST
Get list of all permissions for a user or service. The request has to specify 
either user or service for which to retrieve permissions.


##### CRUD

POST: /api/permissions
Request: <permission>
Privilege: ADMIN
Create or modify permission. Permissions are matched by service and username.

DELETE: /api/permissions
Request: <permission>
Delete permission. Permissions are matched by service and username.


#### Files

{
  filename: "string",
  fileSize: integer,
  timeCreated: "ISO8601 DateTime",
  sha1: "string",
  compressedFileSize: integer,
}

##### List

GET: /api/services/{serviceName}/files[?pattern={regular-expression}]
Response: [ <file>, ... ]
Privilege: LIST
List files under particular service, optionally filtering them using regular
expression.

##### CRUD

POST: /api/services/{serviceName}/files/{filename}
Request: binary file stream
Respnse: <file>
Privilege: WRITE
Upload file to specified service.

GET: /api/services/{serviceName}/files/{filename}
Response: binary file stream
Privilege: READ
Download file from service. 

DELETE: /api/services/{serviceName}/files/{filename}
Privilege: ADMIN
Delete file from service-id.


##### Audit

{
  user: "string",
  userId: integer,
  ip: "string",
  action: integer,
  timestamp: "ISO8601 DateTime",
  stringDetail1: "string",
  stringDetail2: "string",
  stringDetail3: "string",
  longDetail1: integer,
  longDetail2: integer,
  longDetail3: integer,
  doubleDetail1: real,
  doubleDetail2: real,
  doubleDetail3: real,
}

##### List

GET: /api/audit
Response: [ <audit>, ... ]
Privilege: SYSADMIN
Retrieve audit log.
