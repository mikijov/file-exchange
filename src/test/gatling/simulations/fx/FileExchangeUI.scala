package fx

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class FileExchangeUI extends Simulation {

  val fileFeeder = new Feeder[String] {
    import scala.util.Random
    private val RNG = new Random

    private val FILES = Array(
      "1kb.txt", "1kb.txt",
      "10kb.txt", "10kb.txt", "10kb.txt",
      "100kb.txt", "100kb.txt", "100kb.txt", "100kb.txt", "100kb.txt",
      "1mb.txt", "1mb.txt", "1mb.txt", "1mb.txt", "1mb.txt", "1mb.txt",
      "10mb.txt", "10mb.txt", "10mb.txt", "10mb.txt", "10mb.txt",
      "100mb.txt", "100mb.txt", "100mb.txt",
      "1gb.txt")

    // random number in between [a...b]
    private def randInt(a:Int, b:Int) = RNG.nextInt(b-a) + a

    // always return true as this feeder can be polled infinitively
    override def hasNext = true

    override def next: Map[String, String] = {
      val localFile = FILES(randInt(0, FILES.length))
      val remoteFile = localFile + "." + randInt(1, 1000000000) + ".txt"

      Map("localFile" -> localFile,
        "remoteFile" -> remoteFile)
    }
  }

  val httpConf = http
    .baseURL("http://localhost:8080")
    .contentTypeHeader("application/json")
    .inferHtmlResources(
      BlackList(
        """.*\.css""", """.*\.js""", """.*\.png""", """.*\.jpg""", """.*\.gif""", 
        """.*\.eof""", """.*\.ttf""", """.*\.svg""", """.*\.woff""", 
        """.*\.woff2""", """.*\.gz""", """.*\.otf""", """.*\.html"""), 
      WhiteList()
    )

  // ~10 seconds
  object Login {
    val login = exec(http("check old credentials")
      .get("/file-exchange/api/session/user")
      .check(status.is(401)))
      .pause(5)

      .exec(http("failed login")
      .get("/file-exchange/api/session/user")
      .basicAuth("admin","wrongpassword")
      .check(status.is(401)))
      .pause(5)

      .exec(http("login")
      .get("/file-exchange/api/session/user")
      .basicAuth("admin","password"))

      .repeat(5) {
        exec(http("waiting on dashboard to navigate further")
          .get("/file-exchange/api/stats")
          .basicAuth("admin","password"))
          .pause(1)
      }
  }


  // ~60 seconds
  object Dashboard {
    val dashboard = 
      repeat(60) {
        exec(http("monitoring dashboard for 1 min")
          .get("/file-exchange/api/stats")
          .basicAuth("admin","password"))
          .pause(1)
      }
  }

  // ~35 seconds
  object Users {
    val users = exec(http("inspect user and modify permissions")
      .get("/file-exchange/api/users")
      .basicAuth("admin","password"))
      .pause(9)

      .exec(http("user details")
      .get("/file-exchange/api/users/user.39")
      .basicAuth("admin","password")
      .resources(http("user permissions")
      .get("/file-exchange/api/users/user.39/permissions")
      .basicAuth("admin","password")))
      .pause(15)

      .exec(http("add permission")
      .post("/file-exchange/api/services/test.service.33/permissions")
      .body(StringBody("{\"username\":\"user.39\",\"service\":\"test.service.33\",\"permission\":\"2\"}"))
      .basicAuth("admin","password")
      .check(status.in(204)))
      .pause(5)

      .exec(http("remove permission")
      .delete("/file-exchange/api/services/test.service.33/permissions/user.39")
      .basicAuth("admin","password")
      .check(status.in(204, 404)))
      .pause(5)
  }

  // ~30 seconds
  object Services {
    val services = exec(http("list services")
      .get("/file-exchange/api/services")
      .basicAuth("admin","password"))
      .pause(3)

      .exec(http("list files")
      .get("/file-exchange/api/services/test.service.18/files")
      .basicAuth("admin","password"))
      .pause(7)

      .exec(http("filter files 1")
      .get("/file-exchange/api/services/test.service.18/files?pattern=leng")
      .basicAuth("admin","password"))
      .pause(1)

      .exec(http("filter files 2")
      .get("/file-exchange/api/services/test.service.18/files?pattern=length10.")
      .basicAuth("admin","password"))
      .pause(2)

      .exec(http("filter files 3")
      .get("/file-exchange/api/services/test.service.18/files?pattern=length10.11")
      .basicAuth("admin","password"))
      .pause(9)

      .exec(http("list services")
      .get("/file-exchange/api/services")
      .basicAuth("admin","password"))
      .pause(3)

      .exec(http("list files")
      .get("/file-exchange/api/services/testCreateFile/files")
      .basicAuth("admin","password"))
      .pause(5)
  }

  // ~5 seconds
  object Monitors {
    val monitors = exec(http("list monitor")
      .get("/file-exchange/api/monitors")
      .basicAuth("admin","password"))
      .pause(2)

      .exec(http("view monitor details")
      .get("/file-exchange/api/monitors/testCreateMonitor")
      .basicAuth("admin","password"))
      .pause(3)
  }

  // ~1 second
  object Settings {
    val settings = exec(http("view settings")
      .get("/file-exchange/api/settings")
      .basicAuth("admin","password"))
      .pause(1)
  }
  
  // 1 minute
  object CliPollForFile {
    val clipollforfile = 
      repeat(6) {
        exec(http("polling for file")
          .get("/file-exchange/api/services/application.1.input/files/will.never.arrive")
          .basicAuth("admin","password")
          .check(status.in(404)))
        .pause(10)
      }
  }

  object CliUploadFile {
    val cliuploadfile = 
      feed(fileFeeder)
      .exec(http("uploading file ${localFile}")
        .post("/file-exchange/api/services/application.1.input/files/${remoteFile}")
        .body(RawFileBody("${localFile}"))
        .basicAuth("admin","password"))
  }

  object CliDownloadFile {
    val clidownloadfile = 
      feed(fileFeeder)
      .exec(http("downloading file ${localFile}")
        .get("/file-exchange/api/services/application.1.input/files/${localFile}")
        .basicAuth("admin","password"))
  }

  val rtbMonitor = scenario("RtbMonitor").exec(
    Login.login, 
    Dashboard.dashboard)

  val addPermission = scenario("AddPermission").exec(
    Login.login, 
    Users.users)

  val serviceAndFiles = scenario("ServiceAndFiles").exec(
    Login.login, 
    Services.services)

  val monitors = scenario("Monitors").exec(
    Login.login, 
    Monitors.monitors)

  val settings = scenario("Settings").exec(
    Login.login, 
    Settings.settings)

  val pollingApp = scenario("Application waiting for a file").exec(
    CliPollForFile.clipollforfile)

  val uploadingApp = scenario("Application uploading a file").exec(
    CliUploadFile.cliuploadfile)

  val downloadingApp = scenario("Application downloading a file").exec(
    CliDownloadFile.clidownloadfile)

  // setUp(settings.inject(atOnceUsers(1))).protocols(httpConf)
  setUp(
    pollingApp.inject(
      constantUsersPerSec(20) during(20 seconds) randomized
    ),
    uploadingApp.inject(
      constantUsersPerSec(1) during(60 seconds) randomized
    ),
    downloadingApp.inject(
      constantUsersPerSec(2) during(60 seconds) randomized
    ),
    rtbMonitor.inject(
      atOnceUsers(1)
    ),
    addPermission.inject(
      atOnceUsers(1)
    ),
    serviceAndFiles.inject(
      constantUsersPerSec(1) during(5 seconds) randomized
    ),
    monitors.inject(
      constantUsersPerSec(1) during(20 seconds) randomized
    )
  ).protocols(httpConf)

}
