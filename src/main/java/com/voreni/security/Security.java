package com.voreni.security;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Security
{
    public static final double CLOCKS_PER_YEAR_PER_GHZ =
                    1000000000.0 * 60 * 60 * 24 * 365.4;

	public static double calculatePasswordStrength(String password)
	{
		int multiplier = 0; // estimated key space
		String temp = password;

		if (password.matches(".*[a-z].*"))
		{
			multiplier += 26;
			temp = temp.replaceAll("[a-z]", "");
		}
		if (password.matches(".*[A-Z].*"))
		{
			multiplier += 26;
			temp = temp.replaceAll("[A-Z]", "");
		}
		if (password.matches(".*[0-9].*"))
		{
			multiplier += 10;
			temp = temp.replaceAll("[0-9]", "");
		}
		if (password.matches(".*\\p{Punct}.*"))
		{
			multiplier += 32;
			temp = temp.replaceAll("\\p{Punct}", "");
		}
		// if any other characters are used, strengthen the key space somewhat
		if (temp.length() > 0)
		{
			multiplier += 10; // 10 is an estimate
		}

		// Calculate effective password length.
		// Any characters that repeat themselves immediately don't count.
		temp = password;
		for (int i = 0; i < temp.length() - 1; )
		{
			if (temp.charAt(i) == temp.charAt(i + 1))
			{
				temp = temp.substring(0, i).concat(temp.substring(i + 1));
			}
			else
			{
				++i;
			}
		}
		// Any char sequences count as one.
		for (int i = 0; i < temp.length() - 1; )
		{
			int end;
			for (end = i + 1; end < temp.length(); ++end)
			{
				if (
						(temp.charAt(end) == temp.charAt(end - 1) + 1) ||
						(temp.charAt(end) == temp.charAt(end - 1) - 1)
					)
				{
					continue;
				}
				break;
			}
			if (end - i >= 3)
			{
				temp = temp.substring(0, i + 1).concat(temp.substring(end));
			}
			else
			{
				++i;
			}
		}
		// Characters that repeat themselves separately count max 2 times.
		int effectiveLength = 0;
		while (temp.length() > 0)
		{
			char ch = temp.charAt(0);
			effectiveLength += 1;

			if (temp.indexOf(ch, 1) != -1) // does the character repeat?
			{
				effectiveLength += 1; // allow only one repetition, any more does not count
			}

			temp = temp.substring(1);
			for (int i = temp.indexOf(ch); i != -1; i = temp.indexOf(ch))
			{
				temp = temp.substring(0, i).concat(temp.substring(i + 1));
			}
		}

		// estimate number of instructions to crack the password
		return Math.pow(multiplier, effectiveLength);
	}

	//
	// AES Encryption/Decryption
	//

	/*
	 * http://jerryorr.blogspot.ca/2012/05/secure-password-storage-lots-of-donts.html
	 *
	 * The flow goes something like this:
	 *
	 * 1. When adding a new user, call generateSalt(), then hashPassword(), and store both
	 * the encrypted password and the salt. Do not store the clear-text password. Don't
	 * worry about keeping the salt in a separate table or location from the encrypted
	 * password; as discussed above, the salt is non-secret.
	 *
	 * 2. When authenticating a user, call hashPassword() with user entered password and
	 * salt, and compare the result with the previously stored hashed password.
	 *
	 * 3. When a user changes their password, it's safe to reuse their old salt; you can
	 * just call hashPassword() with the old salt.
	 */

	public static final int AES_BLOCK_SIZE = 128;

	/**
	 * Salt is randomly generated value, used to prolong key calculation and can be
	 * transmitted in the clear.
	 *
	 * @param bytes Number of bytes of salt to generate. 8 bytes (64 bits) is a usual size
	 * (recommended by RSA PKCS5).
	 */
	public static byte[] generateSalt(int bytes) throws NoSuchAlgorithmException
	{
		// Very important to use SecureRandom instead of just Random.
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

		byte[] salt = new byte[bytes];
		random.nextBytes(salt);

		return salt;
	}

	/**
	 * Hash the password using provided salt and predetermined number of iterations.
	 *
	 * Hashed passwords are fit to be stored in the database. Hashed passwords are used
	 * to generate session encryption keys.
	 */
	public static byte[] hashPassword(String password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException
	{
		// PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST specifically names
		// SHA-1 as an acceptable hashing algorithm for PBKDF2
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		// 1974 - Randomly chosen number of iterations. NIST recommends at least 1000, and
		// iOS reportedly uses 10000.
		// 160 is the standard hash size produced by SHA1; but we need 128 bit AES keys
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1974, AES_BLOCK_SIZE);
		return factory.generateSecret(spec).getEncoded();
	}

	/**
	 * Generate random key used for symmetric encryption.
	 *
	 * Such keys are for example useful as session keys.
	 */
	public static SecretKey aesGenerateKey() throws NoSuchAlgorithmException
	{
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(AES_BLOCK_SIZE);
		return keyGenerator.generateKey();
	}

	/**
	 * Generated encryption key by first hashing the password and then converting it into
	 * the encryption key.
	 */
	public static SecretKey aesKeyFromPassword(String password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException
	{
		return new SecretKeySpec(hashPassword(password, salt), "AES");
	}

	/**
	 * Generates encryption key from a hashed password.
	 */
	public static SecretKey aesKeyFromHashedPassword(byte[] hashedPassword)
			throws NoSuchAlgorithmException, InvalidKeySpecException
	{
		return new SecretKeySpec(hashedPassword, "AES");
	}

	/**
	 * Encrypts a byte array using provided key using symmetrical 128 bit AES.
	 */
	public static byte[] aesEncrypt(SecretKey key, byte[] msg)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException
	{
		// TODO: To improve security, use CBC instead of ECB, but then another parameter
		// needs to be passed around, the IV (initialization vector). IV can be sent in
		// clear text, so it is only a matter of adding parameters.

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(msg);
	}

	/**
	 * Decrypts a byte array using provided key using symmetrical 128 bit AES.
	 */
	public static byte[] aesDecrypt(SecretKey key, byte[] msg)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException
	{
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(msg);
	}

	//
	// RSA Public/Private encryption
	//

	public static KeyPair rsaGenerateKey() throws NoSuchAlgorithmException
	{
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048);
		return generator.genKeyPair();
	}

	public static PublicKey rsaPublicKeyFromByteArray(byte[] rawKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(rawKey));
	}

	public static PrivateKey rsaPrivateKeyFromByteArray(byte[] rawKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(rawKey));
	}

	public static byte[] rsaEncrypt(PublicKey key, byte[] msg)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException
	{
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(msg);
	}

	public static byte[] rsaDecrypt(PrivateKey key, byte[] msg)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException
	{
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(msg);
	}
}
