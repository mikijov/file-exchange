(function() {

  "use strict";

  angular.module("fxServices", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/services", {
      templateUrl: "services.html",
      controller: "servicesCtrl"
    });
  }])

  .controller("servicesCtrl", ["$scope", "$log", "fxApi", "$location", "$timeout",
              function($scope, $log, fxApi, $location, $timeout) 
  {
    $scope.$parent.viewName = "services";
    $scope.services = [];
    $scope.filter = "";

    $scope.onEditService = function(service) {
      $scope.current = service;
      $scope.oldServiceName = service.name;
    };

    $scope.onSaveService = function(service) {
      fxApi.updateService($scope.oldServiceName, service)
      .then(
        function() {
          $scope.current = null;
          $scope.errorMessage = null;
        },
        function(response) {
          $log.info("Problem saving service.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onCancelService = function(service) {
      service.name = $scope.oldServiceName;
      $scope.current = null;
    };

    $scope.onDeleteService = function(service) {
      fxApi.deleteService($scope.oldServiceName)
      .then(
        function() {
          $scope.current = null;
          $scope.errorMessage = null;
          var index = $scope.services.indexOf(service);
          $scope.services.splice(index, 1);
        },
        function(response) {
          $log.info("Problem deleting service.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onAdd = function() {
      $scope.newService = {
        name: "",
      };
    };

    $scope.onCancel = function() {
      $scope.newService = null;
      $scope.errorMessage = null;
    };

    $scope.onSave = function() {
      fxApi.createService($scope.newService)
      .then(
        function() {
          $scope.services.push($scope.newService);
          $scope.newService = null;
          $scope.errorMessage = null;
        },
        function(response) {
          $log.info("Problem saving service.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onFilter = function() {
      $timeout.cancel($scope.filterTimer);
      $scope.filterTimer = $timeout($scope.refresh, 500);
    };

    $scope.refresh = function() {
      fxApi.getServices($scope.filter)
      .then(
        function(services) {
          $log.info("Services:", angular.toJson(services));
          $scope.services = services;
        },
        function(response) {
          $log.error("Failed to get services.", response.status, response.statusText);
        }
      );
    };

    $scope.onServiceClick = function(user) {
      $location.url("/services/" + user.name + "/files");
    };

    $scope.refresh();
  }]);

})();
