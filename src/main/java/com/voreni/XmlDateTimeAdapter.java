package com.voreni;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.joda.time.DateTime;

/**
 * JAXB XmlAdapter to marshal JodaTime.DateTime to and from
 * ISO 8601 string format.
 * <p>
 * To use it, simply annotate the DateTime member like this:
 * <pre>
 * @XmlJavaTypeAdapter(
 *     type=DateTime.class,
 *     value=DateTimeAdapter.class)
 * </pre>
 */
public class XmlDateTimeAdapter
    extends XmlAdapter<String, DateTime>
{
    public DateTime unmarshal(String value)
        throws Exception
    {
        return new DateTime(value);
    }

    public String marshal(DateTime value)
        throws Exception
    {
        return value.toString();
    }
}