(function() {

  "use strict";

  angular.module("fxUsers", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/users", {
      templateUrl: "users.html",
      controller: "usersCtrl"
    });
  }])

  .controller("usersCtrl", ["$scope", "$log", "fxApi", "$location", "$timeout",
              function($scope, $log, fxApi, $location, $timeout) 
  {
    $scope.$parent.viewName = "users";
    $scope.users = [];
    $scope.filter = "";

    $scope.onAdd = function() {
      $location.url("/users/!new");
    };

    $scope.onFilter = function() {
      $timeout.cancel($scope.filterTimer);
      $scope.filterTimer = $timeout($scope.refresh, 500);
    };

    $scope.onUserClick = function(user) {
      $location.url("/users/" + user.username);
    };

    $scope.refresh = function() {
      fxApi.getUsers($scope.filter)
      .then(
        function(users) {
          $log.info("Users:", angular.toJson(users));
          $scope.users = users;
        },
        function(response) {
          $log.error("Failed to get users.", response.status, response.statusText);
        }
      );
    };

    $scope.refresh();
  }]);

})();
