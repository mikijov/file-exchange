package com.voreni.fileserver;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.voreni.fileserver.api.Ws;

public class Auth {
	private final static Logger LOGGER = Logger.getLogger(Auth.class.getName());

    public static enum Authorization {
        AUTHENTICATED,
        SELF,
        LIST,
        READ,
        WRITE,
        ADMIN,
        SYSADMIN;
    }

    public static Authorization getAuthorization(User user) {
        if (user.isSysAdmin()) {
            return Authorization.SYSADMIN;
        }
        else {
            return Authorization.AUTHENTICATED;
        }
    }

    /**
     * Calculate what authorization on the provided service is the user,
     * the owner of the session, is allowed.
     * @param serviceName for which to calculate authorization.
     * @return Authorization of the user over specified service.
     * @throws SQLException in case of database error.
     */
    public static Authorization getServiceAuthorization(User user, String serviceName)
                    throws SQLException
    {
        if (user.isSysAdmin()) {
            return Authorization.SYSADMIN;
        }
        int permission = Db.getServicePermission(serviceName, user.getId());
        switch (permission) {
            case DbInterface.LIST:
                return Authorization.LIST;
            case DbInterface.READ:
                return Authorization.READ;
            case DbInterface.WRITE:
                return Authorization.WRITE;
            case DbInterface.ADMIN:
                return Authorization.ADMIN;
        }

        // even though its authenticated, it is not authorized
        return Authorization.AUTHENTICATED;
    }

    /**
     * Calculate what authorization on the provided user is the user,
     * the owner of the session, is allowed. I.e. is the user SYSADMIN,
     * is he operating on himself, or merely AUTHENTICATED.
     * @param username for which to calculate authorization.
     * @return Authorization of the user over specified user.
     * @throws SQLException in case of database error.
     */
    public static Authorization getUserAuthorization(User user, String username) {
        // check user
        if (user.isSysAdmin()) {
            return Authorization.SYSADMIN;
        }
        else if (user.getUsername().equals(username)) {
            return Authorization.SELF;
        }

        // even though its authenticated, it is not authorized
        return Authorization.AUTHENTICATED;
    }

    public static void checkPermission(User user, Authorization minimumPrivilege)
    {
        try {
            Authorization authorization = getAuthorization(user);

            if (authorization.ordinal() < minimumPrivilege.ordinal()) {
                LOGGER.info("Not authorized.  (" + authorization.name() + " < "
                                + minimumPrivilege.name() + ")");
                throw new WebApplicationException(
                    "User does not have provilege to access this resource.",
                    Response.Status.FORBIDDEN);
            }
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    public static void checkPermission(User user, String serviceName,
        Authorization minimumPrivilege)
    {
        try {
            Authorization authorization = getServiceAuthorization(user, serviceName);

            if (authorization.ordinal() < minimumPrivilege.ordinal()) {
                LOGGER.info("Not authorized.  (" + authorization.name() + " < "
                                + minimumPrivilege.name() + ")");
                throw new WebApplicationException(
                    "User does not have provilege to access this resource.",
                    Response.Status.FORBIDDEN);
            }
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    public static void checkUserPermission(User user, String username,
        Authorization minimumPrivilege)
    {
        try {
            Authorization authorization = getUserAuthorization(user, username);

            if (authorization.ordinal() < minimumPrivilege.ordinal()) {
                LOGGER.info("Not authorized.  (" + authorization.name() + " < "
                                + minimumPrivilege.name() + ")");
                throw new WebApplicationException(
                    "User does not have provilege to access this resource.",
                    Response.Status.FORBIDDEN);
            }
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }
}
