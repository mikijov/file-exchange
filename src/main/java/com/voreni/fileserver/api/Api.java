package com.voreni.fileserver.api;

import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api/*")
public class Api extends ResourceConfig {
	private final static Logger LOGGER = Logger.getLogger(Api.class.getName());

	/*
	 * Web service components are being registered here. However application
	 * lifetime is being controlled in ServletContextListener.
	 */
    public Api() {
        LOGGER.config("Configuring API Servlet.");

        packages("com.voreni.fileserver.api");
        register(AuthFilter.class);
        register(MyJacksonFeature.class);
        register(MultiPartFeature.class);
    }
}
