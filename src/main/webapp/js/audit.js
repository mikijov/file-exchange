(function() {

  "use strict";

  angular.module("fxAudit", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/audit", {
      templateUrl: "audit.html",
      controller: "auditCtrl"
    });
  }])

  .controller("auditCtrl", ["$scope", "$log", "fxApi",
              function($scope, $log, fxApi) 
  {
    $scope.$parent.viewName = "audit";
    $scope.audit = [];
    $scope.filter = "";

    $scope.refresh = function() {
      fxApi.getAudit($scope.filter)
      .then(
        function(audit) {
          $log.info("Audit:", angular.toJson(audit));
          $scope.audit = audit;
        },
        function(response) {
          $log.error("Failed to get services.", response.status, response.statusText);
        }
      );
    };

    $scope.refresh();
  }]);

})();
