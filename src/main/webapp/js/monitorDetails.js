(function() {

  "use strict";

  angular.module("fxMonitorDetails", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/monitors/:monitorName", {
      templateUrl: "monitorDetails.html",
      controller: "monitorDetailsCtrl"
    });
  }])

  .controller("monitorDetailsCtrl", [
    "$scope", "$log", "fxApi", "$routeParams", "$location", "$interval",
    function($scope, $log, fxApi, $routeParams, $location, $interval) 
  {
    $scope.$parent.viewName = "monitors";
    $scope.editing = false;

    $scope.getStatusClass = function(status) {
      if (status == "Waiting") {
        return "bg-yellow";
      }
      else if (status == "Arrived") {
        return "bg-green";
      }
      else if (status) {
        return "bg-red";
      }
      else {
        return "";
      }
    };

    $scope.onRefresh = function() {
      // TODO: change timer to 15 seconds; 1 sec used only for demo
      $interval.cancel($scope.timer);
      $scope.refresh();
      $scope.timer = $interval($scope.refresh, 1000);
    };

    $scope.onEdit = function() {
      $interval.cancel($scope.timer);
      $scope.editing = true;
    };

    $scope.onSave = function() {
      var promise;
      if ($scope.isNew) {
        promise = fxApi.createMonitor($scope.monitor);
      }
      else {
        promise = fxApi.updateMonitor($routeParams.monitorName, $scope.monitor);
      }
      promise.then(
        function(response) {
          $log.info("Monitor saved.");
          $scope.editing = false;
          $scope.errorMessage = null;
          $scope.refresh();
          $scope.timer = $interval($scope.refresh, 1000);
        },
        function(response) {
          $log.info("Problem saving monitor.");
          $scope.errorMessage = response.statusText;
        }
      );
    };

    $scope.onCancel = function() {
      $scope.editing = false;
      if ($scope.isNew) {
        $location.url("/monitors");
      }
      else {
        $scope.load();
      }
    };

    $scope.onAddFeed = function() {
      $scope.monitor.feeds.push(
        {
          service: "",
          pattern: "",
        }
      );
    };

    $scope.refresh = function() {
      for (var feed of $scope.monitor.feeds) {
        fxApi.getFiles(feed.service, feed.pattern)
        .then(
          function(feed) {
            return function(files) {
              $log.info(feed.pattern + " matched: " + angular.toJson(files));
              switch (files.length) {
                case 0:
                  feed.status = "Waiting";
                  break;
                case 1:
                  feed.status = "Arrived";
                  break;
                default:
                  feed.status = "Multiple";
                  break;
              }
            };
          }(feed),
          function(response) {
            $log.error("Failed to match files.", response.status, response.statusText);
          }
        );
      }
    };

    $scope.load = function() {
      fxApi.getMonitor($routeParams.monitorName)
      .then(
        function(monitor) {
          $log.info("Monitor:", angular.toJson(monitor));
          $scope.monitor = monitor;
          $scope.refresh();
        },
        function(response) {
          $log.error("Failed to get monitor.", response.status, response.statusText);
        }
      );
    };

    // When the DOM element is removed from the page,
    // AngularJS will trigger the $destroy event on
    // the scope. This gives us a chance to cancel any
    // pending timer that we may have.
    $scope.timer = $interval($scope.refresh, 1000);
    $scope.$on(
      "$destroy",
      function(event) {
        $interval.cancel($scope.timer);
      }
    );

    $scope.isNew = $routeParams.monitorName == "!new";
    if ($scope.isNew) {
      $scope.monitor = {
        name:"",
        startOffset:0,
        feeds:[],
      };
      $scope.editing = true;
    }
    else {
      $scope.load();
    }

  }]);

})();
