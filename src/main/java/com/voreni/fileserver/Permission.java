package com.voreni.fileserver;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@JsonAutoDetect(
    creatorVisibility=JsonAutoDetect.Visibility.ANY,
    fieldVisibility=JsonAutoDetect.Visibility.NONE,
    getterVisibility=JsonAutoDetect.Visibility.NONE,
    isGetterVisibility=JsonAutoDetect.Visibility.NONE,
    setterVisibility=JsonAutoDetect.Visibility.NONE
    )
public class Permission {
    @JsonProperty
    private String username;
    @JsonProperty
    private String service;
    @JsonProperty
    private int permission = DbInterface.NONE;

    public Permission() {
    }

    public Permission(String service, String username) {
        this.service = service;
        this.username = username;
    }

    public Permission(String service, String username, int permission) {
        this.service = service;
        this.username = username;
        this.permission = permission;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getService() {
        return service;
    }

    public void setService(String salt) {
        this.service = salt;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    @Override
    @SuppressFBWarnings
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        Permission p = (Permission)other;
        return (username == p.getUsername() || username.equals(p.getUsername()))
                && (service == p.getService() || service.equals(p.getService()))
                && (permission == p.getPermission());
    }
}
