package com.voreni.security;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class SecurityTest
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	public static void main(String[] args)
	{
		System.out.println(String.format("%s = %.2f",
				"abcd", Security.calculatePasswordStrength("abcd")));
		System.out.println(String.format("%s = %.2f",
				"395tRlaBababRoc", Security.calculatePasswordStrength("395tRlaBababRoc")));

		try
		{
			System.out.println();
			System.out.println("AES");
			byte[] salt = Security.generateSalt(8);
			String password = "395tRlaBababRoc";
			byte[] hashedPassword = Security.hashPassword(password, salt);
			System.out.println(String.format("395tRlaBababRoc(%s) = %s",
					DatatypeConverter.printHexBinary(salt),
					DatatypeConverter.printHexBinary(hashedPassword)));
			salt = Security.generateSalt(8);
			hashedPassword = Security.hashPassword(password, salt);
			System.out.println(String.format("395tRlaBababRoc(%s) = %s",
					DatatypeConverter.printHexBinary(salt),
					DatatypeConverter.printHexBinary(hashedPassword)));

			password = "abcd";
			hashedPassword = Security.hashPassword(password, salt);
			System.out.println(String.format("abcd(%s) = %s",
					DatatypeConverter.printHexBinary(salt),
					DatatypeConverter.printHexBinary(hashedPassword)));
			salt = Security.generateSalt(8);
			hashedPassword = Security.hashPassword(password, salt);
			System.out.println(String.format("abcd(%s) = %s",
					DatatypeConverter.printHexBinary(salt),
					DatatypeConverter.printHexBinary(hashedPassword)));

			System.out.println("AES key: " +
					DatatypeConverter.printHexBinary(Security.aesGenerateKey().getEncoded()));
			System.out.println("AES key: " +
					DatatypeConverter.printHexBinary(Security.aesGenerateKey().getEncoded()));
			System.out.println("AES key from pw(\"abcd\"): " +
					DatatypeConverter.printHexBinary(Security.aesKeyFromPassword("abcd", salt).getEncoded()));
			System.out.println("AES key from pw(\"abcd\"): " +
					DatatypeConverter.printHexBinary(Security.aesKeyFromPassword("abcd", salt).getEncoded()));
			System.out.println("AES key from hash(\"abcd\"): " +
					DatatypeConverter.printHexBinary(Security.aesKeyFromHashedPassword(hashedPassword).getEncoded()));
			System.out.println("AES key from hash(\"abcd\"): " +
					DatatypeConverter.printHexBinary(Security.aesKeyFromHashedPassword(hashedPassword).getEncoded()));

			SecretKey key = Security.aesKeyFromHashedPassword(hashedPassword);

			final byte[] in = { 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 1, 1, 1};
			System.out.println(DatatypeConverter.printHexBinary(in));
			byte[] enc = Security.aesEncrypt(key, in);
			System.out.println(DatatypeConverter.printHexBinary(enc));
			enc = Security.aesEncrypt(key, in);
			System.out.println(DatatypeConverter.printHexBinary(enc));
			byte[] dec = Security.aesDecrypt(key, enc);
			System.out.println(DatatypeConverter.printHexBinary(dec));

			System.out.println();
			System.out.println("RSA");

			KeyPair kp = Security.rsaGenerateKey();
			byte[] publicBytes = kp.getPublic().getEncoded();
			byte[] privateBytes = kp.getPrivate().getEncoded();
			System.out.println(String.format("Public(%s): %s",
					kp.getPublic().getFormat(),
					DatatypeConverter.printHexBinary(publicBytes)));
			System.out.println(String.format("Private(%s): %s",
					kp.getPrivate().getFormat(),
					DatatypeConverter.printHexBinary(privateBytes)));

			String text = "Some text to be encrypted.";
			System.out.println(text);

			PublicKey publicKey = Security.rsaPublicKeyFromByteArray(publicBytes);
			PrivateKey privateKey = Security.rsaPrivateKeyFromByteArray(privateBytes);

			enc = Security.rsaEncrypt(publicKey, text.getBytes());
			System.out.println(String.format("Enc: %s",
					DatatypeConverter.printHexBinary(enc)));
			dec = Security.rsaDecrypt(privateKey, enc);
			System.out.println(String.format("Dec: %s",
					DatatypeConverter.printHexBinary(dec)));

			enc = Security.rsaEncrypt(publicKey, text.getBytes());
			System.out.println(String.format("Enc: %s",
					DatatypeConverter.printHexBinary(enc)));
			dec = Security.rsaDecrypt(privateKey, enc);
			System.out.println(String.format("Dec: %s",
					DatatypeConverter.printHexBinary(dec)));
			dec = Security.rsaDecrypt(privateKey, enc);
			System.out.println(String.format("Dec: %s",
					DatatypeConverter.printHexBinary(dec)));

			text = new String(dec, "UTF-8");
			System.out.println(text);
		}
		catch (GeneralSecurityException e)
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
	}
}
