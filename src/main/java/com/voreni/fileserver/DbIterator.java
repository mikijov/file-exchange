package com.voreni.fileserver;

import java.sql.SQLException;

// TODO: Ensure all DBIterator usage is properly released/closed by using try(iter = ...
public interface DbIterator<T> extends AutoCloseable {
    boolean hasNext() throws SQLException;
    T next() throws SQLException;
}
