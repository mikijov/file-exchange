package com.voreni.fileserver;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.voreni.fileserver.api.MyJacksonFeature;

public class Audit
    extends TimerTask
{
    private final static Logger LOGGER = Logger.getLogger(Audit.class.getName());

    public static final int USER_CREATE = 10;
    public static final int USER_READ = 11;
    public static final int USER_UPDATE = 12;
    public static final int USER_DELETE = 13;
    public static final int USER_LIST = 14;
    public static final int USER_PASSWORD = 15;

    public static final int SERVICE_CREATE = 20;
    public static final int SERVICE_READ = 21;
    public static final int SERVICE_UPDATE = 22;
    public static final int SERVICE_DELETE = 23;
    public static final int SERVICE_LIST = 24;

    public static final int PERMISSION_CREATE = 30;
    public static final int PERMISSION_READ = 31;
    public static final int PERMISSION_UPDATE = 32;
    public static final int PERMISSION_DELETE = 33;
    public static final int PERMISSION_LIST = 34;

    public static final int FILE_CREATE = 40;
    public static final int FILE_READ = 41;
    public static final int FILE_UPDATE = 42;
    public static final int FILE_DELETE = 43;
    public static final int FILE_LIST = 44;

    public static final int MONITOR_CREATE = 50;
    public static final int MONITOR_READ = 51;
    public static final int MONITOR_UPDATE = 52;
    public static final int MONITOR_DELETE = 53;
    public static final int MONITOR_LIST = 54;

//    public static final int AUDIT_CREATE = 90;
//    public static final int AUDIT_READ = 91;
//    public static final int AUDIT_UPDATE = 92;
//    public static final int AUDIT_DELETE = 93;
    public static final int AUDIT_LIST = 94;

    private static final int FLUSH_INTERVAL = 1000; // milliseconds

    @JsonAutoDetect(
        creatorVisibility=JsonAutoDetect.Visibility.ANY,
        fieldVisibility=JsonAutoDetect.Visibility.NONE,
        getterVisibility=JsonAutoDetect.Visibility.NONE,
        isGetterVisibility=JsonAutoDetect.Visibility.NONE,
        setterVisibility=JsonAutoDetect.Visibility.NONE
        )
    public static class Entry {
        @JsonProperty
        String user;
        @JsonProperty
        long userId;
        @JsonProperty
        String ip;
        @JsonProperty
        int action;
        @JsonProperty
        @JsonFormat(pattern=MyJacksonFeature.ISO8601_FORMAT)
        DateTime timestamp;
        @JsonProperty
        String stringDetail1 = null;
        @JsonProperty
        String stringDetail2 = null;
        @JsonProperty
        String stringDetail3 = null;
        @JsonProperty
        long longDetail1 = 0;
        @JsonProperty
        long longDetail2 = 0;
        @JsonProperty
        long longDetail3 = 0;
        @JsonProperty
        double doubleDetail1 = 0;
        @JsonProperty
        double doubleDetail2 = 0;
        @JsonProperty
        double doubleDetail3 = 0;
    }

    private static final Audit singleton = new Audit();
    private static final Timer timer = new Timer("Audit Runner", true);

    private static boolean quit = false;
    private static List<Entry> entries = new ArrayList<Entry>();
    private static List<Entry> savingEntries = new ArrayList<Entry>();

    private static DateTime timestamp = DateTime.now();

    public static void initialize() {
        timer.schedule(singleton, FLUSH_INTERVAL, FLUSH_INTERVAL);
    }

    public static void done() {
        quit = true;
        try {
            synchronized (Audit.class) {
                Audit.class.wait(FLUSH_INTERVAL * 2);
            }
        }
        catch (InterruptedException e) {
            LOGGER.warning("Did not properly synchrinize while waiting for Audit thread to finish. A memory leak is possible.");
        }
    }

    public static void log(Entry entry) {
        synchronized (Audit.class) {
            entries.add(entry);
        }
    }

    @Override
    public void run() {
        if (quit) {
            timer.cancel();
            synchronized (Audit.class) {
                Audit.class.notifyAll();
            }
            return;
        }

        synchronized (Audit.class) {
            timestamp = DateTime.now();
            List<Entry> temp = entries;
            entries = savingEntries;
            savingEntries = temp;
        }

        if (!savingEntries.isEmpty()) {
            try {
                Db.saveAudit(savingEntries);
            }
            catch (SQLException e) {
                LOGGER.log(Level.SEVERE, "Unable to save audit log.", e);
            }
            savingEntries.clear();
        }
    }

    public static void log(User user, String ip, int action) {
        Entry entry = new Entry();
        entry.user = user.getUsername();
        entry.userId = user.getId();
        entry.ip = ip;
        entry.action = action;
        entry.timestamp = timestamp;

        log(entry);
    }

    public static void log(User user, String ip, int action, String detail1) {
        Entry entry = new Entry();
        entry.user = user.getUsername();
        entry.userId = user.getId();
        entry.ip = ip;
        entry.action = action;
        entry.timestamp = timestamp;
        entry.stringDetail1 = detail1;

        log(entry);
    }

    public static void log(User user, String ip, int action, String detail1, long longDetail1) {
        Entry entry = new Entry();
        entry.user = user.getUsername();
        entry.userId = user.getId();
        entry.ip = ip;
        entry.action = action;
        entry.timestamp = timestamp;
        entry.stringDetail1 = detail1;
        entry.longDetail1 = longDetail1;

        log(entry);
    }

    public static void log(User user, String ip, int action, String detail1, String detail2) {
        Entry entry = new Entry();
        entry.user = user.getUsername();
        entry.userId = user.getId();
        entry.ip = ip;
        entry.action = action;
        entry.timestamp = timestamp;
        entry.stringDetail1 = detail1;
        entry.stringDetail2 = detail2;

        log(entry);
    }

    public static void log(User user, String ip, int action, String detail1, String detail2, long longDetail1) {
        Entry entry = new Entry();
        entry.user = user.getUsername();
        entry.userId = user.getId();
        entry.ip = ip;
        entry.action = action;
        entry.timestamp = timestamp;
        entry.stringDetail1 = detail1;
        entry.stringDetail2 = detail2;
        entry.longDetail1 = longDetail1;

        log(entry);
    }
}
