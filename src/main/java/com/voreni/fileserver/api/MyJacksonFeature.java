package com.voreni.fileserver.api;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * Registers a customized {@link JacksonJaxbJsonProvider}.
 */
public class MyJacksonFeature implements Feature
{
    public static final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static final DateTimeFormatter isoFormatter =
                    DateTimeFormat.forPattern(ISO8601_FORMAT);

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static {
        // We want ISO dates, not Unix timestamps!:
        OBJECT_MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        OBJECT_MAPPER.registerModule(new JodaModule());
    };

    private static final JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
    static {
        provider.setMapper(OBJECT_MAPPER);
    };

    /** This method is what actually gets called,
        when your ResourceConfig registers a Feature. */
    @Override
    public boolean configure(FeatureContext context) {
        context.register(provider);
        return true;
    }
}