package com.voreni.fileserver.api;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.voreni.fileserver.User;

@Path("session")
public class SessionWebService {
	private final static Logger LOGGER = Logger.getLogger(SessionWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser()
    {
        LOGGER.fine("getUser()");

        return (User)request.getProperty(AuthFilter.USER);
    }

}
