package com.voreni.fileserver.api;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import com.voreni.fileserver.Db;
import com.voreni.fileserver.Stats;
import com.voreni.fileserver.User;
import com.voreni.security.Security;

//@Provider
@PreMatching
public class AuthFilter implements ContainerRequestFilter
{
	private final static Logger LOGGER = Logger.getLogger(AuthFilter.class.getName());

	public final static String USER = "voreni.user";
	public final static String IP = "voreni.ip";

    public static boolean isValidPassword(User user, String password)
                    throws WebApplicationException
    {
        try {
            byte[] hashedPassword = Security.hashPassword(
                password,
                DatatypeConverter.parseHexBinary(user.getSalt())
            );
            byte[] expectedPassword = DatatypeConverter.parseHexBinary(user.getPassword());

            return Arrays.equals(hashedPassword, expectedPassword);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.log(Level.SEVERE, "Internal server error.", e);
            throw new WebApplicationException(
                "Internal server error. Please contact support.",
                Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Context
    private HttpServletRequest request;

    public String getClientIpAddr() {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * Apply the filter : check input request, validate or not with user auth
     * @param request The request from Tomcat server
     */
    @Override
    public void filter(ContainerRequestContext request) throws WebApplicationException {
        LOGGER.info(request.getMethod() + ": " + request.getUriInfo().getPath());

        Stats.incApiCall();
        if ("settings".equals(request.getUriInfo().getPath()) && !Db.isInitialized()) {
            return;
        }
        Ws.checkServer();

        String authorization = request.getHeaderString(HttpHeaders.AUTHORIZATION);

        // tell user to authenticate if he did not provide credentials
        if (authorization == null || authorization.length() < 6) {
            // TODO: allow changing realm
            request.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                // including WWW-Authenticate will prompt browser to ask for password in a popup
                // excluding it will allow js code to handle password itself.
//                .header("WWW-Authenticate", "Basic realm=\"file-exchange:\"")
                .build());
            return;
        }

        String temp = authorization.substring(0, 6);
        if (!temp.equalsIgnoreCase("Basic ")) {
            throw new WebApplicationException(
                "Cannot process Authorization header parameter.",
                Response.Status.BAD_REQUEST);
        }
        authorization = authorization.substring(6).trim();
        try {
            authorization = new String(DatatypeConverter.parseBase64Binary(authorization));
        }
        catch (IllegalArgumentException e) {
            throw new WebApplicationException(
                "Cannot process Authorization header parameter.",
                Response.Status.BAD_REQUEST);
        }
        String[] credentials = authorization.split(":");
        if (credentials.length != 2) {
            throw new WebApplicationException(
                "Cannot process Authorization header parameter.",
                Response.Status.BAD_REQUEST);
        }
        String username = credentials[0];
        String password = credentials[1];

        User user;
        try {
            user = Db.getUser(username);
        }
        catch (SQLException e) {
            if (Db.isNotFoundError(e)) {
                LOGGER.info("Invalid login attempt for user " + username);
                throw new WebApplicationException(
                    "Invalid credentials.",
                    Response.Status.UNAUTHORIZED);
            }
            else {
                LOGGER.log(Level.SEVERE, "Internal server error.", e);
                throw new WebApplicationException(
                    "Internal server error. Please contact support.",
                    Response.Status.INTERNAL_SERVER_ERROR);
            }
        }

        if (!isValidPassword(user, password)) {
            LOGGER.info("Invalid login attempt for user " + username);
            throw new WebApplicationException(
                "Invalid credentials.",
                Response.Status.UNAUTHORIZED);
        }

        request.setProperty(USER, user);
        request.setProperty(IP, getClientIpAddr());
    }
}