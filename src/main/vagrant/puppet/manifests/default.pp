########################
# General Firewall Setup
########################

include firewall

# INPUT
#######

firewallchain { 'INPUT:filter:IPv4':
  policy  => 'drop',
}
firewallchain { 'INPUT:filter:IPv6':
  policy  => 'drop',
}
firewall { '001 accept all icmp':
  chain   => 'INPUT',
  proto   => 'icmp',
  action  => 'accept',
}
firewall { '002 accept all to lo interface':
  chain   => 'INPUT',
  proto   => 'all',
  iniface => 'lo',
  action  => 'accept',
}
firewall { '003 accept related established rules':
  chain   => 'INPUT',
  proto   => 'all',
  ctstate => ['RELATED', 'ESTABLISHED'],
  action  => 'accept',
}
# Allow SSH
firewall { '004 allow ssh access':
  chain   => 'INPUT',
  dport   => '22',
  proto   => tcp,
  state   => ['NEW', 'ESTABLISHED'],
  action  => accept,
}
# Allow Tomcat
firewall { '005 allow http-alt access':
  chain   => 'INPUT',
  dport   => '8080',
  proto   => tcp,
  state   => ['NEW', 'ESTABLISHED'],
  action  => accept,
}
firewall { "199 drop all other requests":
  chain   => 'INPUT',
  action  => "drop",
}

# FORWARD
#########

firewallchain { 'FORWARD:filter:IPv4':
  policy  => 'drop',
}
firewallchain { 'FORWARD:filter:IPv6':
  policy  => 'drop',
}
firewall { "499 drop all other requests":
  chain   => 'FORWARD',
  action  => "drop",
}

# OUTPUT
########

firewallchain { 'OUTPUT:filter:IPv4':
  policy   => 'accept',
}
firewallchain { 'OUTPUT:filter:IPv6':
  policy   => 'drop',
}
firewall { '800 accept all to lo interface':
  chain    => 'OUTPUT',
  proto    => 'all',
  outiface => 'lo',
  action   => 'accept',
}
firewall { "899 allow all other requests":
  chain    => 'OUTPUT',
  action   => "accept",
}

# purge unmanaged rules
#######################

resources { "firewall":
  purge => true
}
resources { "firewallchain":
  purge => true
}

# End - Firewall

######
# Java
######

class { 'java': }

########
# Tomcat
########

class { 'tomcat':
  install_from_source => false,
}
tomcat::instance{ 'tomcat7':
  package_name => 'tomcat7',
  # catalina_home => '/usr/share/tomcat7', # common
  # catalina_base => '/var/lib/tomcat7', # instance
}->
tomcat::service { 'tomcat7':
  use_jsvc     => false,
  use_init     => true,
  service_name => 'tomcat7',
}
tomcat::setenv::entry {'JAVA_OPTS':
  config_file => '/usr/share/tomcat7/bin/setenv.sh',
  value => '"$JAVA_OPTS "',
  require => Tomcat::Instance['tomcat7'],
  notify => Tomcat::Service['tomcat7'],
}
tomcat::setenv::entry {'FILE_EXCHANGE_HOME':
  config_file => '/usr/share/tomcat7/bin/setenv.sh',
  value => '/app/file-exchange',
  require => Tomcat::Instance['tomcat7'],
  notify => Tomcat::Service['tomcat7'],
}

# create file-exchange application directory
file { "/app":
  ensure => "directory",
  owner  => "root",
  group  => "root",
  mode   => 755,
}
# create file-exchange application directory
file { "/app/file-exchange":
  ensure => "directory",
  owner  => "tomcat7",
  group  => "tomcat7",
  mode   => 750,
  require => File['/app'],
  notify => Tomcat::Service['tomcat7'],
}
# install file-exchange.war
file { '/var/lib/tomcat7/webapps/file-exchange.war':
  source => '/vagrant/file-exchange.war',
  owner  => "tomcat7",
  group  => "tomcat7",
  mode   => 755,
  require => Tomcat::Instance['tomcat7'],
  notify => Tomcat::Service['tomcat7'],
}

########
# nodejs
########

# class { 'nodejs': }

##########
# Postgres
##########

# class { 'postgresql::globals':
#   encoding => 'UTF-8',
#   locale   => 'en_US.UTF-8',
# }->
# class { 'postgresql::server': 
#   postgres_password => 'TPSrep0rt!',
# }
# postgresql::server::db { 'miki':
#   user     => 'miki',
#   password => 'test1',
# }

###########
# Minecraft
###########

# class { 'minecraft':
#   user => 'minecraft',
#   group => 'minecraft',
#   homedir => '/opt/minecraft',
#   heap_size => 512,
#   heap_start => 256,
#   manage_java => false,
#   manage_screen => false,
#   manage_curl => false,
# }
