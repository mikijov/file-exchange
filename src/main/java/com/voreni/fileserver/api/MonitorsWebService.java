package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.Monitor;
import com.voreni.fileserver.User;

@Path("monitors")
public class MonitorsWebService {
	private final static Logger LOGGER = Logger.getLogger(MonitorsWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    private void checkMonitorName(String name) {
        if (!Pattern.matches("^[a-zA-Z0-9\\.\\-\\_]+$", name)) {
            throw new WebApplicationException(
                "Monitor name must contain only a-z A-Z 0-9 . - _ characters.",
                Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listMonitors(
        @QueryParam("pattern") final String pattern
        )
    {
        LOGGER.fine("listMonitors()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.AUTHENTICATED);

        final Pattern regex = Ws.compilePattern(pattern);

        try {
            final DbIterator<Monitor> iter = Db.getMonitors(null);
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            Monitor monitor = iter.next();
                            if (regex == null || regex.matcher(monitor.getName()).find()) {
                                jg.writeObject(monitor);
                            }
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listMonitors(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            Audit.log(user, ip, Audit.MONITOR_LIST, pattern);

            LOGGER.fine("listMonitors(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createMonitor(
        Monitor monitor
        )
    {
        LOGGER.fine("createMonitor()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        checkMonitorName(monitor.getName());

        try {
            Db.createMonitor(monitor);

            Audit.log(user, ip, Audit.MONITOR_CREATE, monitor.getName());

            LOGGER.fine("createMonitor(" + monitor.getName() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @GET
    @Path("{monitorName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Monitor getMonitor(
        @PathParam("monitorName") String monitorName
        )
    {
        LOGGER.fine("getMonitor()");

        User user = (User)request.getProperty(AuthFilter.USER);
        Auth.checkPermission(user, Auth.Authorization.AUTHENTICATED);

        try {
            return Db.getMonitor(monitorName);
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @PUT
    @Path("{monitorName}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateMonitor(
        @PathParam("monitorName") String monitorName,
        Monitor monitor
        )
    {
        LOGGER.fine("updateMonitor()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        checkMonitorName(monitor.getName());

        try {
            Db.updateMonitor(monitorName, monitor);

            Audit.log(user, ip, Audit.MONITOR_UPDATE, monitorName, monitor.getName());

            LOGGER.fine("updateMonitor(" + monitor.getName() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @DELETE
    @Path("{monitorName}")
    public void deleteMonitor(
        @PathParam("monitorName") String monitorName
        )
    {
        LOGGER.fine("deleteMonitor()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        try {
            Db.deleteMonitor(monitorName);

            Audit.log(user, ip, Audit.MONITOR_DELETE, monitorName);

            LOGGER.fine("deleteMonitor(" + monitorName + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }
}
