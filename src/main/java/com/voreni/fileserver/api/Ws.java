package com.voreni.fileserver.api;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.voreni.fileserver.Db;

public final class Ws {
	private final static Logger LOGGER = Logger.getLogger(Ws.class.getName());

	public static Pattern compilePattern(String pattern) {
	    if (pattern == null) {
	        return null;
	    }

	    try {
	        return Pattern.compile(pattern);
	    }
	    catch (PatternSyntaxException e) {
	        throw new WebApplicationException(
	            "Supplied filter pattern is not valid.",
	            Response.Status.BAD_REQUEST
                );
	    }
	}

    public static WebApplicationException webExceptionFrom(SQLException e) {
        if (Db.isNotFoundError(e)) {
            LOGGER.log(Level.FINE, "Requested item not found.", e);
            throw new WebApplicationException(
                "Requested item not found.",
                Response.Status.NOT_FOUND);
        }
        else if (Db.isDuplicateError(e)) {
            LOGGER.log(Level.INFO, "Attempt to create a duplicate or another constraint problem.", e);
            throw new WebApplicationException(
                "Attempt to create a duplicate or another constraint problem.",
                Response.Status.CONFLICT);
        }
        else {
            LOGGER.log(Level.SEVERE, "Internal server error.", e);
            throw new WebApplicationException(
                "Internal server error. Please contact support.",
                Response.Status.INTERNAL_SERVER_ERROR);
        }

        // never return anything, but allow caller to "throw" result and
        // indicate to compiler that code will always throw
    }

    public static WebApplicationException webExceptionFrom(Exception e) {
        if (e instanceof WebApplicationException) {
            throw (WebApplicationException)e;
        }
        else if (e instanceof IllegalBlockSizeException
                        || e instanceof BadPaddingException
                        )
        {
            LOGGER.info("Invalid session token. Please login again.");
            throw new WebApplicationException(
                "Invalid session token. Please login again.",
                Response.Status.FORBIDDEN);
        }
        else {
            LOGGER.log(Level.SEVERE, "Internal server error.", e);
            throw new WebApplicationException(
                "Internal server error. Please contact support.",
                Response.Status.INTERNAL_SERVER_ERROR);
        }

        // never return anything, but allow caller to "throw" result and
        // indicate to compiler that code will always throw
    }

    public static void checkServer() {
        if (!Db.isInitialized()) {
            LOGGER.severe("Server is not initialized. Settings are not set.");
            throw new WebApplicationException(
                "Server is not initialized. Settings are not set.",
                Response.Status.SERVICE_UNAVAILABLE);
        }
    }
}
