from fx import FileExchange, FileExchangeError
import random
import unittest
import time


random.seed()
PREFIX = '{}.'.format(random.randint(100, 1000))
URL = 'http://localhost:8080/file-exchange/api/'
print "Unit test prefix:", PREFIX


def compareFiles(file1, file2):
    with open(file1) as file:
        leftLines = file.readlines()
    with open(file2) as file:
        rightLines = file.readlines()
    return leftLines == rightLines


class TestBasicApi(unittest.TestCase):
    def testUserApi(self):
        fx = FileExchange(URL, 'admin', 'password')
        username = PREFIX + 'user.testUserApi'
        fx.createUser(username, 'pw1')
        fx.changeUserPassword(username, 'pw2')
        users = fx.listUsers(username + '.*')
        self.assertEqual(len(users), 1)
        self.assertEqual(users[0]['username'], username)
        self.assertFalse(users[0]['isSysAdmin'])
        fx.deleteUser(username)

    def testServiceApi(self):
        fx = FileExchange(URL, 'admin', 'password')
        service = PREFIX + 'service.testServiceApi'
        self.assertEqual(len(fx.listServices(service)), 0)
        fx.createService(service)
        self.assertEqual(len(fx.listServices(service)), 1)
        fx.deleteService(service)

    def testPermissionsApi(self):
        fx = FileExchange(URL, 'admin', 'password')
        username = PREFIX + 'user.testPermissionsApi'
        fx.createUser(username + '.r', 'pw1')
        fx.createUser(username + '.w', 'pw1')
        fx.createUser(username + '.a', 'pw1')
        self.assertEqual(len(fx.listUsers(username + '.*')), 3)

        service = PREFIX + 'service.testPermissionsApi'
        self.assertEqual(len(fx.listServices(service)), 0)
        fx.createService(service)
        self.assertEqual(len(fx.listServices(service)), 1)

        self.assertEqual(len(fx.getServicePermissions(service)), 0)
        fx.createPermission(service, username + '.r', 'R')
        fx.createPermission(service, username + '.w', 'W')
        fx.createPermission(service, username + '.a', 'A')
        self.assertEqual(len(fx.getServicePermissions(service)), 3)

    def testFileApi(self):
        fx = FileExchange(URL, 'admin', 'password')
        service = PREFIX + 'service.testFileApi'
        fx.createService(service)

        before = time.time()
        with self.assertRaises(FileExchangeError):
            fx.download(service + '/sample.txt', 'sample.txt.download', True)
        self.assertGreater(1, abs(time.time() - before))

        before = time.time()
        with self.assertRaises(FileExchangeError):
            fx.download(service + '/sample.txt', 'sample.txt.download', True,
                        timeout=3, retryInterval=1)
        self.assertGreater(1, abs(time.time() - before - 3))

        files = fx.listFiles(service)
        self.assertEqual(len(files), 0)
        fx.upload('sample.txt', service)
        files = fx.listFiles(service)
        self.assertEqual(len(files), 1)
        self.assertEqual(files[0]['filename'], 'sample.txt')

        fx.download(service + '/sample.txt', 'sample.txt.download', True)
        self.assertTrue(compareFiles('sample.txt', 'sample.txt.download'))

        fx.deleteFile(service + '/sample.txt')


class TestPermissions(unittest.TestCase):
    pass


class TestNameAllowedCharacters(unittest.TestCase):
    pass


class TestErrors(unittest.TestCase):
    pass


if __name__ == "__main__":
    unittest.main(verbosity=2)
