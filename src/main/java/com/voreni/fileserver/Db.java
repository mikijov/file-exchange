package com.voreni.fileserver;

import java.sql.SQLException;
import java.util.List;

import org.joda.time.DateTime;

public class Db {
    private static DbInterface singleton = null;

    public static boolean isInitialized() {
        return singleton != null;
    }

    private static DbInterface getSingleton() {
        return singleton;
    }

    public static synchronized void setSingleton(DbInterface singleton) {
        if (Db.singleton != null) {
            throw new IllegalStateException("DbInterface singleton is already set.");
        }
        Db.singleton = singleton;
    }

    public static void close() throws Exception {
        getSingleton().close();
        singleton = null;
    }

    public static void createSchema() throws SQLException {
        getSingleton().createSchema();
    }

    public static DbIterator<User> getUsers() throws SQLException {
        return getSingleton().getUsers(null);
    }

    public static DbIterator<User> getUsers(String pattern) throws SQLException {
        return getSingleton().getUsers(pattern);
    }

    public static User getUser(long id) throws SQLException {
        return getSingleton().getUser(id);
    }

    public static User getUser(String username) throws SQLException {
        return getSingleton().getUser(username);
    }

    public static long createUser(User user) throws SQLException {
        return getSingleton().createUser(user);
    }

    public static void updateUser(String username, User user) throws SQLException {
        getSingleton().updateUser(username, user);
    }

    public static void deleteUser(long id) throws SQLException {
        getSingleton().deleteUser(id);
    }

    public static void deleteUser(String username) throws SQLException {
        getSingleton().deleteUser(username);
    }

    public static DbIterator<Service> getServices() throws SQLException {
        return getSingleton().getServices(null);
    }

    public static DbIterator<Service> getServices(String pattern)
                    throws SQLException
    {
        return getSingleton().getServices(pattern);
    }

    public static long createService(Service service) throws SQLException {
        return getSingleton().createService(service);
    }

    public static void updateService(String serviceName, Service service)
                    throws SQLException
    {
        getSingleton().updateService(serviceName, service);
    }

    public static void deleteService(String serviceName) throws SQLException {
        getSingleton().deleteService(serviceName);
    }

    public static DbIterator<Permission> getPermissions(String service, String username)
                    throws SQLException
    {
        return getSingleton().getPermissions(service, username);
    }

    public static int getServicePermission(String serviceName, long userId)
                    throws SQLException
    {
        return getSingleton().getPermission(serviceName, userId);
    }

    public static void createPermission(Permission permission)
                    throws SQLException
    {
        getSingleton().createPermission(permission);
    }

    public static void deletePermission(Permission permission)
                    throws SQLException
    {
        getSingleton().deletePermission(permission);
    }

    public static DbIterator<File> getFiles(String serviceName, String pattern,
        DateTime fromTime, DateTime toTime)
                        throws SQLException
    {
        return getSingleton().getFiles(serviceName, pattern, fromTime, toTime);
    }

    public static File getFile(String serviceName, String filename) throws SQLException {
        return getSingleton().getFile(serviceName, filename);
    }

    public static void createFile(String serviceName, File file) throws SQLException {
        getSingleton().createFile(serviceName, file);
    }

    public static void deleteFile(String serviceName, String filename) throws SQLException {
        getSingleton().deleteFile(serviceName, filename);
    }

    public static DbIterator<Monitor> getMonitors(String pattern) throws SQLException {
        return getSingleton().getMonitors(pattern);
    }

    public static Monitor getMonitor(String monitorName) throws SQLException {
        return getSingleton().getMonitor(monitorName);
    }

    public static long createMonitor(Monitor monitor) throws SQLException {
        return getSingleton().createMonitor(monitor);
    }

    public static void updateMonitor(String monitorName, Monitor monitor)
                    throws SQLException
    {
        getSingleton().updateMonitor(monitorName, monitor);
    }

    public static void deleteMonitor(String monitorName) throws SQLException {
        getSingleton().deleteMonitor(monitorName);
    }

    public static DbIterator<Audit.Entry> getAudit() throws SQLException {
        return getSingleton().getAudit();
    }
    public static void saveAudit(List<Audit.Entry> entries) throws SQLException {
        getSingleton().saveAudit(entries);
    }

    public static boolean isDuplicateError(SQLException e) {
        return getSingleton().isDuplicateError(e);
    }

    public static boolean isNotFoundError(SQLException e) {
        return getSingleton().isNotFoundError(e);
    }
}
