(function() {

  "use strict";

  angular.module("fxMonitors", [
    "ngRoute",
  ])

  .config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/monitors", {
      templateUrl: "monitors.html",
      controller: "monitorsCtrl"
    });
  }])

  .controller("monitorsCtrl", ["$scope", "$log", "fxApi", "$location", "$timeout",
              function($scope, $log, fxApi, $location, $timeout) 
  {
    $scope.$parent.viewName = "monitors";
    $scope.filter = "";

    $scope.onFilter = function() {
      $timeout.cancel($scope.filterTimer);
      $scope.filterTimer = $timeout($scope.refresh, 500);
    };

    $scope.onAdd = function() {
      $location.url("/monitors/!new");
    };

    $scope.onMonitorClick = function(monitor) {
      $location.url("/monitors/" + monitor.name);
    };

    $scope.refresh = function() {
      fxApi.getMonitors($scope.filter)
      .then(
        function(monitors) {
          $log.info("Monitors:", angular.toJson(monitors));
          $scope.monitors = monitors;
        },
        function(response) {
          $log.error("Failed to get monitors.", response.status, response.statusText);
        }
      );
    };

    $scope.refresh();
  }]);

})();
