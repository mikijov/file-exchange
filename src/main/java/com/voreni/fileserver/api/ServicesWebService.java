package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.Service;
import com.voreni.fileserver.User;

@Path("services")
public class ServicesWebService {
	private final static Logger LOGGER = Logger.getLogger(ServicesWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    private void checkServiceName(String name) {
        if (!Pattern.matches("^[a-zA-Z0-9\\.\\-\\_]+$", name)) {
            throw new WebApplicationException(
                "Service name must contain only a-z A-Z 0-9 . - _ characters.",
                Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listServices(
        @QueryParam("pattern") final String pattern
        )
    {
        LOGGER.fine("listServices()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.AUTHENTICATED);

        final Pattern regex = Ws.compilePattern(pattern);

        try {
            final DbIterator<Service> iter = Db.getServices(null);
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            Service service = iter.next();
                            if (regex == null || regex.matcher(service.getName()).find()) {
                                jg.writeObject(service);
                            }
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listServices(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            Audit.log(user, ip, Audit.SERVICE_LIST, pattern);

            LOGGER.fine("listServices(" + (pattern != null ? "\"" + pattern + "\"" : "") + ") OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @POST
    public void createService(
        Service service
        )
    {
        LOGGER.fine("createService()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        checkServiceName(service.getName());

        try {
            Db.createService(service);

            Audit.log(user, ip, Audit.SERVICE_CREATE, service.getName());

            LOGGER.fine("createService(" + service.getName() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @PUT
    @Path("{serviceName}")
    public void updateService(
        @PathParam("serviceName") String serviceName,
        Service service
        )
    {
        LOGGER.fine("updateService()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        checkServiceName(service.getName());

        try {
            Db.updateService(serviceName, service);

            Audit.log(user, ip, Audit.SERVICE_UPDATE, serviceName, service.getName());

            LOGGER.fine("updateService(" + service.getName() + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

    @DELETE
    @Path("{serviceName}")
    public void deleteService(
        @PathParam("serviceName") String serviceName
        )
    {
        LOGGER.fine("deleteService()");

        User user = (User)request.getProperty(AuthFilter.USER);
        String ip = (String)request.getProperty(AuthFilter.IP);
        Auth.checkPermission(user, serviceName, Auth.Authorization.ADMIN);

        try {
            Db.deleteService(serviceName);

            Audit.log(user, ip, Audit.SERVICE_DELETE, serviceName);

            LOGGER.fine("deleteService(" + serviceName + ") success");
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
    }

}
