package com.voreni.fileserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.JournalMode;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteOpenMode;

// TODO: close statements, result sets etc.

public class DbSqlite extends DbInterface
{
	private final static Logger LOGGER = Logger.getLogger(DbSqlite.class.getName());

    private Connection con;

    public DbSqlite(String filename, boolean allowCreate) throws SQLException {
        initialize(filename, allowCreate);
    }

    public void initialize(String filename, boolean allowCreate) throws SQLException {
        LOGGER.config("DbSqlite.initialize(\"" + filename + "\")");

        try {
            Class.forName("org.sqlite.JDBC");
        }
        catch (ClassNotFoundException e) {
            throw new SQLException("Cannot load sqlite database driver.", e);
        }

        try {
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            config.setJournalMode(JournalMode.WAL);
            if (allowCreate) {
                config.setOpenMode(SQLiteOpenMode.CREATE);
            }
            con = DriverManager.getConnection("jdbc:sqlite:" + filename, config.toProperties());
            con.setAutoCommit(true);
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to open database file \"" + filename + "\".", e);
            throw new SQLException("Failed to open database file \"" + filename + "\".", e);
        }
    }

    @Override
    public void close() throws Exception {
        con.close();
    }

    @Override
    public synchronized void createSchema() throws SQLException {
        LOGGER.config("DbSqlite.createSchema()");

        try (Statement stmt = con.createStatement()) {
            stmt.execute(
                "CREATE TABLE user ( " +
                                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "username TEXT NOT NULL, " +
                                "deleted INTEGER DEFAULT(0) NOT NULL, " +
                                "salt TEXT, " +
                                "password TEXT, " +
                                "sysadmin INTEGER DEFAULT(0) NOT NULL " +
                                ")"
            );
            stmt.execute(
                "CREATE UNIQUE INDEX idx_active_users ON user (username,deleted) WHERE deleted=0"
            );
            stmt.execute("INSERT INTO user (username,salt,password,sysadmin) VALUES ('admin','A25F38A86AA2ADB8','7CBCC0260B2F80CDB5A5B2438D03648D',1)");
            stmt.execute(
                "CREATE TABLE service ( " +
                                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "name TEXT NOT NULL, " +
                                "deleted INTEGER DEFAULT(0) NOT NULL " +
                                ")"
            );
            stmt.execute(
                "CREATE UNIQUE INDEX idx_active_services ON service (name) WHERE deleted=0"
            );
            stmt.execute(
                "CREATE TABLE permission ( " +
                                "id INTEGER PRIMARY KEY, " +
                                "name TEXT NOT NULL " +
                                ")"
            );
            stmt.execute("INSERT INTO permission (id,name) VALUES (1,'LIST')");
            stmt.execute("INSERT INTO permission (id,name) VALUES (2,'READ')");
            stmt.execute("INSERT INTO permission (id,name) VALUES (3,'WRITE')");
            stmt.execute("INSERT INTO permission (id,name) VALUES (4,'ADMIN')");
            stmt.execute(
                "CREATE TABLE service_permission ( " +
                                "service_id INTEGER NOT NULL REFERENCES service(id), " +
                                "user_id INTEGER NOT NULL REFERENCES user(id), " +
                                "permission_id INTEGER NOT NULL REFERENCES permission(id), " +
                                "PRIMARY KEY(service_id,user_id) " +
                                ") WITHOUT ROWID"
            );
            stmt.execute(
                "CREATE TABLE file ( " +
                                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "service_id INTEGER NOT NULL REFERENCES service(id), " +
                                "filename TEXT NOT NULL, " +
                                "file_size INTEGER NOT NULL, " +
                                "compressed_filename TEXT NOT NULL, " +
                                "compressed_file_size INTEGER NOT NULL, " +
                                "checksum TEXT NOT NULL, " +
                                "created_timestamp INTEGER NOT NULL, " +
                                "deleted INTEGER NOT NULL DEFAULT(0) " +
                                ")"
            );
            stmt.execute(
                "CREATE UNIQUE INDEX idx_active_files ON file (service_id, filename) WHERE deleted=0"
            );
            stmt.execute(
                "CREATE TABLE monitor ( " +
                                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "name TEXT NOT NULL, " +
                                "start_offset INTEGER DEFAULT(0) NOT NULL, " +
                                "feeds TEXT" +
                                ")"
            );
            stmt.execute(
                "CREATE UNIQUE INDEX idx_monitors ON monitor (name)"
            );
            stmt.execute(
                "CREATE TABLE audit ( " +
                                "user TEXT NOT NULL, " +
                                "user_id INTEGER NOT NULL, " +
                                "ip TEXT NOT NULL, " +
                                "action INTEGER NOT NULL, " +
                                "timestamp INTEGER NOT NULL, " +
                                "text_detail_1 TEXT, " +
                                "text_detail_2 TEXT, " +
                                "text_detail_3 TEXT, " +
                                "integer_detail_1 INTEGER, " +
                                "integer_detail_2 INTEGER, " +
                                "integer_detail_3 INTEGER, " +
                                "real_detail_1 REAL, " +
                                "real_detail_2 REAL, " +
                                "real_detail_3 REAL " +
                                ")"
            );
        } // try stmt
    }

    public void initializeStats() {
        try (PreparedStatement stmt =
                        con.prepareStatement("SELECT COUNT(*) FROM user WHERE deleted=0")
                )
        {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Stats.setUsers(rs.getLong(1));
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to count users.", e);;
        }

        try (PreparedStatement stmt =
                        con.prepareStatement("SELECT COUNT(*) FROM service WHERE deleted=0")
                )
        {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Stats.setServices(rs.getLong(1));
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to count services.", e);;
        }

        try (PreparedStatement stmt =
                        con.prepareStatement("SELECT COUNT(*), SUM(file_size) FROM file WHERE deleted=0")
                )
        {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Stats.setFiles(rs.getLong(1));
            Stats.setFilesSize(rs.getLong(2));
        }
        catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Failed to count files.", e);;
        }
    }

    @Override
    public synchronized long createUser(User user) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT INTO user (username, salt, password, sysadmin) VALUES (?,?,?,?)"))
        {
            LOGGER.finer("DbSqlite.createUser()");

            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getSalt());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, user.isSysAdmin() ? 1 : 0);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.severe("Failed to create user. No records were inserted.");
                throw new SQLException("Failed to create user. No records were inserted.");
            }

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
            else {
                LOGGER.severe("Creating user failed, no ID obtained.");
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        } // try stmt
    }

    @Override
    public synchronized DbIterator<User> getUsers(String pattern) throws SQLException {
        LOGGER.finer("DbSqlite.getUsers()");

        final PreparedStatement stmt = con.prepareStatement(
            pattern != null ?
                    "SELECT id, username, salt, password, sysadmin FROM user WHERE username LIKE ? AND deleted=0"
                :
                    "SELECT id, username, salt, password, sysadmin FROM user WHERE deleted=0"
                );
        if (pattern != null) {
            stmt.setString(1, pattern);
        }
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<User>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public User next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                User user = new User();
                user.setId(rs.getLong(1));
                user.setUsername(rs.getString(2));
                user.setSalt(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setSysAdmin(rs.getInt(5) != 0);

                hasNext = rs.next();
                return user;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }

    @Override
    public synchronized User getUser(long id) throws SQLException {
        LOGGER.finer("getUser(" + id + ")");

        try (PreparedStatement stmt = con.prepareStatement(
            "SELECT username, salt, password, sysadmin FROM user WHERE id=? AND deleted=0"))
        {
            stmt.setLong(1, id);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setId(id);
                user.setUsername(rs.getString(1));
                user.setSalt(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setSysAdmin(rs.getInt(4) != 0);

                if (rs.next()) {
                    LOGGER.severe("Query unexpectedly returned multiple users.");
                    throw new SQLException("Query unexpectedly returned multiple users.");
                }

                return user;
            }
            else {
                throw new SQLException("User not found. (" + id + ")", "22000", NOT_FOUND);
            }
        } // try stmt
    }

    @Override
    public synchronized User getUser(String username) throws SQLException {
        LOGGER.finer("getUser(\"" + username + "\")");

        try (PreparedStatement stmt = con.prepareStatement(
            "SELECT id, salt, password, sysadmin FROM user WHERE username=? AND deleted=0"))
        {
            stmt.setString(1, username);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setId(rs.getLong(1));
                user.setUsername(username);
                user.setSalt(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setSysAdmin(rs.getInt(4) != 0);

                if (rs.next()) {
                    LOGGER.severe("Query unexpectedly returned multiple users.");
                    throw new SQLException("Query unexpectedly returned multiple users.");
                }

                return user;
            }
            else {
                throw new SQLException(
                    "User not found. (\"" + username + "\")",
                    "22000", NOT_FOUND);
            }
        } // try stmt
    }

    @Override
    public synchronized void deleteUser(long id) throws SQLException {
        LOGGER.finer("deleteUser(" + id + ")");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE user SET deleted=1 WHERE id=? AND deleted=0"))
        {
            stmt.setLong(1, id);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("User not found. (" + id + ")", "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public synchronized void deleteUser(String username) throws SQLException {
        LOGGER.finer("deleteUser(\"" + username + "\")");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE user SET deleted=1 WHERE username=? AND deleted=0"))
        {
            stmt.setString(1, username);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "User not found. (\"" + username + "\")",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public synchronized void updateUser(String username, User user) throws SQLException {
        LOGGER.finer("updateUser(\"" + user.getUsername() + "\":" + user.getId() + ")");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE user SET username=?, salt=?, password=?, sysadmin=? WHERE username=? and deleted=0"))
        {
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getSalt());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, user.isSysAdmin() ? 1 : 0);
            stmt.setString(5, username);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "User not found. (" + user.getId() + ")",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public DbIterator<Service> getServices(String pattern)
        throws SQLException
    {
        LOGGER.finer("DbSqlite.getServices()");

        final PreparedStatement stmt = con.prepareStatement(
            pattern != null ?
                "SELECT id, name FROM service WHERE name LIKE ? AND deleted=0"
                :
                "SELECT id, name FROM service WHERE deleted=0"
            );
        if (pattern != null) {
            stmt.setString(1, pattern);
        }
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<Service>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Service next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                Service service = new Service();
                service.setId(rs.getLong(1));
                service.setName(rs.getString(2));

                hasNext = rs.next();
                return service;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }

    @Override
    public long createService(Service service)
        throws SQLException
    {
        LOGGER.finer("createService(\"" + service.getName() + "\")");

        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT INTO service (name) VALUES (?)"))
        {
            stmt.setString(1, service.getName());

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.severe("Failed to create service. No records were inserted.");
                throw new SQLException("Failed to create service. No records were inserted.");
            }

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
            else {
                LOGGER.severe("Creating service failed, no ID obtained.");
                throw new SQLException("Creating service failed, no ID obtained.");
            }
        } // try stmt
    }

    @Override
    public void updateService(String serviceName, Service service)
        throws SQLException
    {
        LOGGER.finer("updateService(\"" + serviceName + "\")");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE service SET name=? WHERE name=?"))
        {
            stmt.setString(1, service.getName());
            stmt.setString(2, serviceName);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.severe("Failed to update service. No records were updated.");
                throw new SQLException("Failed to update service. No records were updated.");
            }
        } // try stmt
    }

    @Override
    public void deleteService(String serviceName)
        throws SQLException
    {
        LOGGER.finer("deleteService()");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE service SET deleted=1 WHERE name=? AND deleted=0"))
        {
            stmt.setString(1, serviceName);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Service not found. (\"" + serviceName + "\")",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public DbIterator<Permission> getPermissions(final String service, final String username)
        throws SQLException
    {
        LOGGER.finer("DbSqlite.getUserPermissions()");

        String sql = "SELECT service.name AS service_name, permission_id FROM service_permission sp " +
                        "INNER JOIN user ON sp.user_id = user.id AND user.deleted = 0 ";
        if (username != null) {
            sql += "AND user.username = ? ";
        }
        sql += "INNER JOIN service ON sp.service_id = service.id AND service.deleted = 0 ";
        if (service != null) {
            sql += "AND service.name = ?";
        }

        final PreparedStatement stmt = con.prepareStatement(sql);
        if (username != null) {
            stmt.setString(1, username);
        }
        if (service != null) {
            stmt.setString(username != null ? 2 : 1, service);
        }
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<Permission>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Permission next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                Permission permission = new Permission();
                permission.setUsername(username);
                permission.setService(rs.getString(1));
                permission.setPermission(rs.getInt(2));

                hasNext = rs.next();
                return permission;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }

    @Override
    public void createPermission(Permission permission)
        throws SQLException
    {
        LOGGER.finer("authorizeUser()");

        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT OR REPLACE INTO service_permission (service_id, user_id, permission_id) " +
                            "VALUES ( " +
                            "(SELECT id FROM service WHERE name=?), " +
                            "(SELECT id FROM user WHERE username=?), " +
                            "? " +
                            ")"
            ))
        {
            stmt.setString(1, permission.getService());
            stmt.setString(2, permission.getUsername());
            stmt.setInt(3, permission.getPermission());

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Service (\"" + permission.getService() + "\") or user (\"" + permission.getUsername() + "\") not found.",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public void deletePermission(Permission permission)
        throws SQLException
    {
        LOGGER.finer("deauthorizeUser()");

        try (PreparedStatement stmt = con.prepareStatement(
            "DELETE FROM service_permission WHERE " +
                        "service_id IN (SELECT id FROM service WHERE name=?) AND " +
                        "user_id IN (SELECT id FROM user WHERE username=?)"))
        {
            stmt.setString(1, permission.getService());
            stmt.setString(2, permission.getUsername());

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Authorization for " + permission.getService() + ":" + permission.getUsername() + " not found.",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public int getPermission(String serviceName, long userId)
        throws SQLException
    {
        LOGGER.finer("getServicePermission()");

        try (PreparedStatement stmt = con.prepareStatement(
            "SELECT permission_id FROM service_permission WHERE user_id=? AND " +
                        "service_id IN (SELECT id FROM service WHERE name=?)"))
        {
            stmt.setLong(1, userId);
            stmt.setString(2, serviceName);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int permission = rs.getInt(1);

                if (rs.next()) {
                    LOGGER.severe("Query unexpectedly returned multiple users.");
                    throw new SQLException("Query unexpectedly returned multiple permissions.");
                }

                return permission;
            }
            else {
                return NONE;
            }
        }
    }

    @Override
    public void createFile(String serviceName, File file)
        throws SQLException
    {
        LOGGER.finer("createFile()");

        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT INTO file (service_id, filename, file_size, compressed_file_size, compressed_filename, checksum, created_timestamp) " +
                        "VALUES ((SELECT id FROM service WHERE name=? AND deleted=0),?,?,?,?,?,?)"))
        {
            stmt.setString(1, serviceName);
            stmt.setString(2, file.getFilename());
            stmt.setLong(3, file.getFileSize());
            stmt.setLong(4, file.getCompressedFileSize());
            stmt.setString(5, file.getCompressedFilename());
            stmt.setString(6, file.getSha1());
            DateTime dt = file.getTimeCreated();
            if (dt != null) {
                stmt.setLong(7, dt.getMillis());
            }
            else {
                stmt.setNull(7, Types.INTEGER);
            }

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.severe("Failed to create file. No records were inserted.");
                throw new SQLException("Failed to create file. No records were inserted.");
            }
        } // try stmt
    }

    @Override
    public File getFile(String serviceName, String filename)
        throws SQLException
    {
        LOGGER.finer("getFile()");

        try (PreparedStatement stmt = con.prepareStatement(
            "SELECT filename, compressed_filename, created_timestamp, checksum, file_size, compressed_file_size FROM file WHERE " +
                            "filename=? AND deleted=0 AND " +
                            "service_id IN (SELECT id FROM service WHERE name=? AND deleted=0)"))
        {
            stmt.setString(1, filename);
            stmt.setString(2, serviceName);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                File file = new File();
                file.setFilename(rs.getString(1));
                file.setCompressedFilename(rs.getString(2));
                file.setTimeCreated(new DateTime(rs.getLong(3)));
                file.setSha1(rs.getString(4));
                file.setFileSize(rs.getLong(5));
                file.setCompressedFileSize(rs.getLong(6));

                if (rs.next()) {
                    LOGGER.severe("Query unexpectedly returned multiple users.");
                    throw new SQLException("Query unexpectedly returned multiple users.");
                }

                return file;
            }
            else {
                throw new SQLException(
                    "File not found. (\"" + serviceName + "\\" + filename + "\")",
                    "22000", NOT_FOUND);
            }
        } // try stmt
    }

    @Override
    public void deleteFile(String serviceName, String filename)
        throws SQLException
    {
        LOGGER.finer("deleteFile()");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE file SET deleted=1 WHERE filename=? AND deleted=0 AND " +
                        "service_id IN (SELECT id FROM service WHERE name=? AND deleted=0)"))
        {
            stmt.setString(1, filename);
            stmt.setString(2, serviceName);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "File not found. (\"" + serviceName + "\\" + filename + "\")",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public DbIterator<File> getFiles(String serviceName, String pattern,
        DateTime fromTime, DateTime toTime)
                        throws SQLException
    {
        LOGGER.finer("DbSqlite.getFiles()");

        int param = 1;
        String sql =
                        "SELECT filename, compressed_filename, created_timestamp, checksum, " +
                        "file_size, compressed_file_size " +
                        "FROM file " +
                        "WHERE deleted=0 AND " +
                        "service_id IN (SELECT id FROM service WHERE name=? AND deleted=0) ";
        int patternIndex = 0;
        if (pattern != null) {
            sql += "AND filename LIKE ? ";
            patternIndex = ++param;
        }
        int fromTimeIndex = 0;
        if (pattern != null) {
            sql += "AND created_timestamp >= ? ";
            fromTimeIndex = ++param;
        }
        int toTimeIndex = 0;
        if (pattern != null) {
            sql += "AND created_timestamp <= ? ";
            toTimeIndex = ++param;
        }
        sql += "ORDER BY created_timestamp DESC";
//        LOGGER.info(sql);
//        LOGGER.info("Parameters: " + patternIndex + ", " + fromTimeIndex + ", " + toTimeIndex);

        final PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setString(1, serviceName);
        if (patternIndex != 0) {
            stmt.setString(patternIndex, serviceName);
        }
        if (fromTimeIndex != 0) {
            stmt.setLong(fromTimeIndex, fromTime.getMillis());
        }
        if (toTimeIndex != 0) {
            stmt.setLong(toTimeIndex, fromTime.getMillis());
        }
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<File>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public File next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                File file = new File();
                file.setFilename(rs.getString(1));
                file.setCompressedFilename(rs.getString(2));
                file.setTimeCreated(new DateTime(rs.getLong(3)));
                file.setSha1(rs.getString(4));
                file.setFileSize(rs.getLong(5));
                file.setCompressedFileSize(rs.getLong(6));

                hasNext = rs.next();
                return file;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }

    @Override
    public DbIterator<Monitor> getMonitors(String pattern)
        throws SQLException
    {
        LOGGER.finer("DbSqlite.getMonitors()");

        final PreparedStatement stmt = con.prepareStatement(
            pattern != null ?
                "SELECT id, name, start_offset, feeds FROM monitor " +
                    "WHERE name LIKE ? " +
                    "ORDER BY name ASC"
                :
                "SELECT id, name, start_offset, feeds FROM monitor " +
                    "ORDER BY name ASC"
        );
        if (pattern != null) {
            stmt.setString(1, pattern);
        }
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<Monitor>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Monitor next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                Monitor monitor = new Monitor();
                monitor.setId(rs.getLong(1));
                monitor.setName(rs.getString(2));
                monitor.setStartOffset(new Duration(rs.getLong(3)));
                monitor.setFeeds(rs.getString(4));

                hasNext = rs.next();
                return monitor;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }

    @Override
    public Monitor getMonitor(String monitorName)
        throws SQLException
    {
        LOGGER.finer("getMonitor()");

        try (PreparedStatement stmt = con.prepareStatement(
            "SELECT id, name, start_offset, feeds FROM monitor WHERE name=?"))
        {
            stmt.setString(1, monitorName);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Monitor monitor = new Monitor();
                monitor.setId(rs.getLong(1));
                monitor.setName(rs.getString(2));
                monitor.setStartOffset(new Duration(rs.getLong(3)));
                monitor.setFeeds(rs.getString(4));

                if (rs.next()) {
                    LOGGER.severe("Query unexpectedly returned multiple monitors.");
                    throw new SQLException("Query unexpectedly returned multiple monitors.");
                }

                return monitor;
            }
            else {
                throw new SQLException(
                    "Monitor not found. (\"" + monitorName + "\")",
                    "22000", NOT_FOUND);
            }
        } // try
    }

    @Override
    public long createMonitor(Monitor monitor)
        throws SQLException
    {
        LOGGER.finer("createMonitor()");

        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT INTO monitor (name, start_offset, feeds) VALUES (?,?,?)"
            ))
        {
            stmt.setString(1, monitor.getName());
            stmt.setLong(2, monitor.getStartOffset().getMillis());
            stmt.setString(3, monitor.getFeeds());

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                LOGGER.severe("Failed to create monitor. No records were inserted.");
                throw new SQLException("Failed to create monitor. No records were inserted.");
            }

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
            else {
                throw new SQLException("Creating monitor failed, no ID obtained.");
            }
        }
    }

    @Override
    public void updateMonitor(String monitorName, Monitor monitor)
        throws SQLException
    {
        LOGGER.finer("updateMonitor()");

        try (PreparedStatement stmt = con.prepareStatement(
            "UPDATE monitor SET name=?, start_offset=?, feeds=? WHERE name=?"
            ))
        {
            stmt.setString(1, monitor.getName());
            stmt.setLong(2, monitor.getStartOffset().getMillis());
            stmt.setString(3, monitor.getFeeds());
            stmt.setString(4, monitorName);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Failed to update monitor. Monitor not found.", "22000", NOT_FOUND);
            }
        }
    }

    @Override
    public void deleteMonitor(String monitorName)
        throws SQLException
    {
        LOGGER.finer("deleteMonitor()");

        try (PreparedStatement stmt = con.prepareStatement(
            "DELETE FROM monitor WHERE name=?"))
        {
            stmt.setString(1, monitorName);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException(
                    "Monitor " + monitorName + " not found.",
                    "22000", NOT_FOUND);
            }
            assert affectedRows == 1;
        }
    }

    @Override
    public DbIterator<Audit.Entry> getAudit()
        throws SQLException
    {
        LOGGER.finer("DbSqlite.getAudit()");

        final PreparedStatement stmt = con.prepareStatement(
            "SELECT user, user_id, ip, action, timestamp, " +
                            "text_detail_1, text_detail_2, text_detail_3, " +
                            "integer_detail_1, integer_detail_2, integer_detail_3, " +
                            "real_detail_1, real_detail_2, real_detail_3 " +
                            "FROM audit"
        );
        final ResultSet rs = stmt.executeQuery();

        return new DbIterator<Audit.Entry>() {
            private boolean hasNext = rs.next();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Audit.Entry next() throws SQLException {
                if (!hasNext) {
                    return null;
                }

                Audit.Entry entry = new Audit.Entry();
                entry.user = rs.getString(1);
                entry.userId = rs.getLong(2);
                entry.ip = rs.getString(3);
                entry.action = rs.getInt(4);
                entry.timestamp = new DateTime(rs.getLong(5));
                entry.stringDetail1 = rs.getString(6);
                entry.stringDetail2 = rs.getString(7);
                entry.stringDetail3 = rs.getString(8);
                entry.longDetail1 = rs.getLong(9);
                entry.longDetail2 = rs.getLong(10);
                entry.longDetail3 = rs.getLong(11);
                entry.doubleDetail1 = rs.getDouble(12);
                entry.doubleDetail2 = rs.getDouble(13);
                entry.doubleDetail3 = rs.getDouble(14);

                hasNext = rs.next();
                return entry;
            }

            @Override
            public void close() throws Exception
            {
                stmt.close();
            }
        };
    }
    @Override
    public void saveAudit(List<Audit.Entry> entries)
        throws SQLException
    {
        try (PreparedStatement stmt = con.prepareStatement(
            "INSERT INTO audit " +
                        "(user, user_id, ip, action, timestamp, text_detail_1, text_detail_2, text_detail_3, " +
                        "integer_detail_1, integer_detail_2, integer_detail_3, " +
                        "real_detail_1, real_detail_2, real_detail_3) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"))
        {
            for (Audit.Entry entry: entries) {
                stmt.setString(1, entry.user);
                stmt.setLong(2, entry.userId);
                stmt.setString(3, entry.ip);
                stmt.setInt(4, entry.action);
                stmt.setLong(5, entry.timestamp.getMillis());
                stmt.setString(6, entry.stringDetail1);
                stmt.setString(7, entry.stringDetail2);
                stmt.setString(8, entry.stringDetail3);
                stmt.setLong(9, entry.longDetail1);
                stmt.setLong(10, entry.longDetail2);
                stmt.setLong(11, entry.longDetail3);
                stmt.setDouble(12, entry.doubleDetail1);
                stmt.setDouble(13, entry.doubleDetail2);
                stmt.setDouble(14, entry.doubleDetail3);
                stmt.addBatch();
            }
            stmt.executeBatch();
        }
    }

    private int NOT_FOUND = -1;

    @Override
    public boolean isDuplicateError(SQLException e) {
        return e.getErrorCode() == SQLiteErrorCode.SQLITE_CONSTRAINT.code;
    }

    @Override
    public boolean isNotFoundError(SQLException e) {
        return e.getErrorCode() == NOT_FOUND;
    }
}
