package com.voreni.fileserver.api;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.voreni.fileserver.Audit;
import com.voreni.fileserver.Auth;
import com.voreni.fileserver.Db;
import com.voreni.fileserver.DbIterator;
import com.voreni.fileserver.User;

@Path("audit")
public class AuditWebService {
	private final static Logger LOGGER = Logger.getLogger(AuditWebService.class.getName());

	@Context
	private ContainerRequestContext request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAudit()
    {
        LOGGER.fine("listAudit()");

        User user = (User)request.getProperty(AuthFilter.USER);
        Auth.checkPermission(user, Auth.Authorization.SYSADMIN);

        try {
            final DbIterator<Audit.Entry> iter = Db.getAudit();
            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException
                {
                    try {
                        JsonGenerator jg = MyJacksonFeature.OBJECT_MAPPER
                                        .getFactory()
                                        .createGenerator(os, JsonEncoding.UTF8);

                        jg.writeStartArray();
                        while (iter.hasNext()) {
                            Audit.Entry entry = iter.next();
                            jg.writeObject(entry);
                        }
                        jg.writeEndArray();

                        jg.flush();
                        jg.close();

                        LOGGER.fine("listAudit() Finished");
                    }
                    catch (SQLException e) {
                        throw Ws.webExceptionFrom(e);
                    }
                    finally {
                        try {
                            iter.close();
                        }
                        catch (Exception e) {
                            throw Ws.webExceptionFrom(e);
                        }
                    }
                }
            };

            LOGGER.fine("listAudit() OK");

            return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
        }
        catch (SQLException e) {
            throw Ws.webExceptionFrom(e);
        }
        catch (Exception e) {
            throw Ws.webExceptionFrom(e);
        }
    }
}
